package main

import (
	"fmt"
	"io/ioutil"
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/mergetb/mcc/pkg"

	xir "gitlab.com/mergetb/xir/v0.3/go"

	"gitlab.com/mergetb/mcc/pkg/checkers"
	"gitlab.com/mergetb/mcc/pkg/reticulators"
)

// Version ...
var Version = "undefined"

var (
	logLevel string

	root = &cobra.Command{
		Use:   "mcc",
		Short: "Invoke checkers and reticulators against network models",
	}
)

func init() {
	root.PersistentFlags().StringVarP(&logLevel, "logLevel", "l", "info", "Set logging level")
	loginit()
}

func main() {

	cobra.EnablePrefixMatching = true

	version := &cobra.Command{
		Use:   "version",
		Short: "display version and exit",
		Args:  cobra.NoArgs,
		Run: func(*cobra.Command, []string) {
			fmt.Println(Version)
		},
	}
	root.AddCommand(version)

	list := &cobra.Command{
		Use:   "list",
		Short: "list available checkers and reticulators",
		Args:  cobra.NoArgs,
		Run: func(*cobra.Command, []string) {
			loginit()
			listEm()
		},
	}
	root.AddCommand(list)

	// build individual commands
	for _, r := range mcc.Reticulators() {
		name := r.Name()
		rec := r
		cmd := &cobra.Command{
			Use:   fmt.Sprintf("%s [filename]", name),
			Short: fmt.Sprintf("Run the %s reticulator", name),
			Args:  cobra.MinimumNArgs(0),
			Run: func(cmd *cobra.Command, args []string) {
				loginit()
				if len(args) == 0 {
					runReticulator(rec, "")
				} else {
					runReticulator(rec, args[0])
				}
			},
		}
		root.AddCommand(cmd)
	}

	for _, c := range mcc.Checkers() {
		name := c.Name()
		chk := c
		cmd := &cobra.Command{
			Use:   fmt.Sprintf("%s [filename]", name),
			Short: fmt.Sprintf("Run the %s checker", name),
			Args:  cobra.MinimumNArgs(0),
			Run: func(cmd *cobra.Command, args []string) {
				loginit()
				if len(args) == 0 {
					runChecker(chk, "")
				} else {
					runChecker(chk, args[0])
				}
			},
		}
		root.AddCommand(cmd)
	}

	autocomplete := &cobra.Command{
		Use:   "autocomplete",
		Short: "Generates bash completion scripts",
		Run: func(cmd *cobra.Command, args []string) {
			root.GenBashCompletion(os.Stdout)
		},
	}
	root.AddCommand(autocomplete)

	root.Execute()
}

func loginit() {
	l, err := log.ParseLevel(logLevel)
	if err != nil {
		log.SetLevel(log.InfoLevel)
	} else {
		log.SetLevel(l)
	}
}

func listEm() {

	log.Trace("list called")

	fmt.Print("Checkers: ")
	for _, c := range mcc.Checkers() {
		fmt.Printf("%s ", c.Name())
	}
	fmt.Print("\n")

	fmt.Print("Reticulators: ")
	for _, r := range mcc.Reticulators() {
		fmt.Printf("%s ", r.Name())
	}
	fmt.Print("\n")
}

func runReticulator(r reticulators.ModelReticulator, filename string) {

	log.Tracef("runReticulator %s %s", r.Name(), filename)

	x := getXir(filename)

	ok, err := r.Reticulate(x)
	if err != nil {
		log.Fatal(err)
	}

	if !ok {
		log.Warn("Reticulator was not ok")
		return
	}

	out, err := x.ToB64Buf()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("%s\n", out)
}

func runChecker(c checkers.ModelChecker, filename string) {

	log.Tracef("runChecker %s %s", c.Name(), filename)

	x := getXir(filename)

	ok, diagnostics, err := c.Check(x)
	if err != nil {
		if diagnostics != nil && len(*diagnostics) > 0 {
			fmt.Println("Error - diagnostics follow")
			for _, d := range *diagnostics {
				fmt.Printf("----\n%s", d)
			}
		}
		log.Fatal(err)
	}

	if !ok {
		// TODO parse diagnostics
		log.Warn("Checker was not ok")
		fmt.Printf("Diagnostics: %+v", diagnostics)
		return
	}

	out, err := x.ToB64Buf()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("%s\n", out)
}

func getXir(filename string) *xir.Network {

	var buf []byte
	var err error

	if filename != "" {

		buf, err = ioutil.ReadFile(filename)
		if err != nil {
			log.Fatalf("unable to read file %s: %v", filename, err)
		}

	} else {

		buf, err = ioutil.ReadAll(os.Stdin)
		if err != nil {
			log.Fatal(err)
		}
	}

	net, err := xir.NetworkFromB64String(string(buf))
	if err != nil {
		log.Fatalf("Error building xir net from b64 encoded file: %v", err)
	}

	return net

}

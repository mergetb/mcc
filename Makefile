
cmds = mcc

rwildcard=$(wildcard $1$2) $(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2))

VERSION = $(shell git describe --always --long --dirty)

cmdbin = $(patsubst %,build/%,$(cmds))
cmdcode = $(patsubst %,cmd/%,$(cmds))
pkgcode = $(call rwildcard,pkg/,*.go)

all: $(cmdbin)

build/%: $(cmdcode)
	$(go-build-cmd)

build:
	$(QUIET) mkdir build

$(cmdbin): $(pkgcode)

test:
	go test ./... $(TESTQUIET)

clean:
	$(QUIET) $(RM) -rf build

BLUE=\e[34m
GREEN=\e[32m
CYAN=\e[36m
NORMAL=\e[39m

QUIET=@
TESTQUIET=
ifeq ($(V),1)
	QUIET=
	TESTQUIET=-v
endif

define build-slug
	@echo -e "$(BLUE)$1$(GREEN)\t $< $(CYAN)$@$(NORMAL)"
endef

define go-build-cmd
	$(call build-slug,go)
	$(QUIET) cd $<; \
		go build \
		-ldflags="-X main.Version=$(VERSION)" \
		-o ../../$@
endef


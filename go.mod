module gitlab.com/mergetb/mcc

go 1.20

require (
	github.com/dolthub/swiss v0.2.1
	github.com/edwingeng/deque/v2 v2.1.1
	github.com/gammazero/workerpool v1.1.3
	github.com/puzpuzpuz/xsync/v3 v3.0.2
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/cobra v1.1.3
	gitlab.com/mergetb/xir v0.3.18
	gonum.org/v1/gonum v0.11.0
	k8s.io/apimachinery v0.29.1
)

require (
	github.com/dolthub/maphash v0.1.0 // indirect
	github.com/dustin/go-humanize v1.0.0 // indirect
	github.com/gammazero/deque v0.2.0 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/exp v0.0.0-20231110203233-9a3e6036ecaa // indirect
	golang.org/x/sys v0.15.0 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
)

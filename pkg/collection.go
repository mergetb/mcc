package mcc

import (
	"gitlab.com/mergetb/mcc/pkg/checkers"
	"gitlab.com/mergetb/mcc/pkg/reticulators"
)

// Checkers returns the full list of available checkers.
func Checkers() []checkers.ModelChecker {

	return []checkers.ModelChecker{
		&checkers.Sanity{},
		&checkers.NetworkIslands{},
		&checkers.MPLLimits{},
		&checkers.NodeSelfLink{},
		&checkers.ValidAddresses{},
	}
}

// Reticulators returns the full list of reticulators.
func Reticulators() []reticulators.ModelReticulator {

	ipad := reticulators.NewIPv4Addressing()
	sr := &reticulators.StaticRouting{}

	// Order is important.
	// Routing depends on all addresses exsiting for instance. THe user
	// can add addresses or have the addressing reticulator add them.
	return []reticulators.ModelReticulator{
		ipad,
		sr,
	}
}

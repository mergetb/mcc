#!/usr/bin/bash

PY2IR=/usr/bin/py2xir.py

for f in "$@"; do
    if [[ $f == *.py ]] ; then
        cat $1 | python3 $PY2IR | ../build/mcc ipv4-addressing > ${f%.py}.b64
    fi
done

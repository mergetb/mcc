package errors

import (
	"errors"
)

var (
	// static routing
	ErrNoAddressOnEndpoint = errors.New("endpoint has no address")
	ErrDuplicateSubnets    = errors.New("duplicate subnets assigned across links")
	ErrMultipleLinkSubnets = errors.New("multiple subnets in a single link")
	ErrInvalidNetwork      = errors.New("invalid topology for routing")

	// MPL Limits
	ErrTooManyLinks = errors.New("too many links in MPL") // it takes a lot to make a stew...

	// Node Self link
	ErrNodeSelfLink = errors.New("node self link")

	// Valid Addresses
	ErrInvalidAddress = errors.New("invalid address")

	// Sanity checks
	ErrNoNodes         = errors.New("topology has no nodes")
	ErrInvalidNodeName = errors.New("invalid node name")
)

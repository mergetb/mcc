from mergexp import *

net = Network('testing')
a = [net.node('a%d'%i) for i in range(3)]
a_net = net.connect(a)

a_net[a[0]].socket.addrs = ip4('10.0.1.48/32')
a_net[a[1]].socket.addrs = ip4('10.0.1.47')

experiment(net)

import mergexp as mx

net = mx.Network('nodeselflink')

node = net.node("node")
another = net.node("another")
andanother = net.node("andanother")

net.connect([node, another, andanother])

mx.experiment(net)

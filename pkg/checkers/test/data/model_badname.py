from mergexp import *

# create the network topology object
net = Network('example network')

# Create nodes and add them to the network
nodes = [net.node(n) for n in ['valid', 'not valid', 'cat🐈cat']]

# Connect nodes
link = net.connect(nodes)

# Execute the topology
experiment(net)

from mergexp import *

# Create a netwok topology object.
net = Network('nodeselflink')

node = net.node("node")
another = net.node("another")

net.connect([node, another])
net.connect([node, node])

experiment(net)

package checkers

import (
	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/xir/v0.3/go"
)

// NetworkIslands checks a network model for network islands
type NetworkIslands struct {
}

// Name ...
func (ni *NetworkIslands) Name() string {
	return "network-island"
}

// Check implements the model check interface.
func (ni *NetworkIslands) Check(x *xir.Network) (bool, *[]ModelDiagnostic, error) {
	log.Warn("Network Islands model check implementation is TBD.")
	return true, nil, nil
}

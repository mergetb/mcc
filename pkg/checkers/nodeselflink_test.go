package checkers

import (
	"errors"
	"io/ioutil"
	"testing"

	xir "gitlab.com/mergetb/xir/v0.3/go"

	merr "gitlab.com/mergetb/mcc/pkg/errors"
)

func fromFile(path string, t *testing.T) *xir.Network {

	buf, err := ioutil.ReadFile(path)
	if err != nil {
		t.Fatal(err)
	}

	net, err := xir.NetworkFromB64String(string(buf))
	if err != nil {
		t.Fatal(err)
	}

	return net
}

func TestNodeSelfLink(t *testing.T) {

	x := fromFile("test/data/nodeselflink.b64", t)

	c := &NodeSelfLink{}
	ok, diags, err := c.Check(x)

	if ok {
		t.Fatal("failed to find node self link")
	}

	if len(*diags) != 1 {
		t.Fatal("unexpected number of node self link diagnostics")
	}

	if (*diags)[0].Node != "node" {
		t.Fatal("unexpected node self link")
	}

	if !errors.Is(err, merr.ErrNodeSelfLink) {
		t.Fatal(err)
	}
}

func TestNoNodeSelfLink(t *testing.T) {

	x := fromFile("test/data/no-nodeselflink.b64", t)
	c := &NodeSelfLink{}
	ok, diags, err := c.Check(x)

	if !ok {
		t.Fatal("check should pass")
	}

	if diags != nil {
		t.Fatal("unexpected node self link diagnostics")
	}

	if err != nil {
		t.Fatal(err)
	}
}

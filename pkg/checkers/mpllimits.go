package checkers

import (
	"fmt"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/xir/v0.3/go"

	merr "gitlab.com/mergetb/mcc/pkg/errors"
)

const (
	MAX_MPL_LINKS = 64
)

// MPL Limits checks that MPLs are acceptible
type MPLLimits struct {
}

// Name ...
func (ml *MPLLimits) Name() string {
	return "mpllimits"
}

func (ml *MPLLimits) Check(x *xir.Network) (bool, *[]ModelDiagnostic, error) {
	log.Info("Checking MPL Limits of the model")

	var diags []ModelDiagnostic

	for _, l := range x.GetLinks() {
		if len(l.Endpoints) > MAX_MPL_LINKS {
			diags = append(diags, ModelDiagnostic{
				Message: fmt.Sprintf("link %s has %d links. max is %d",
					l.Id, len(l.Endpoints), MAX_MPL_LINKS),
				Constraint: "too many links",
				Link:       l.Id,
			})
		}
	}

	if len(diags) > 0 {
		return false, &diags, merr.ErrTooManyLinks
	}

	return true, nil, nil
}

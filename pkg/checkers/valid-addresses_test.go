package checkers

import (
	"errors"
	"testing"

	merr "gitlab.com/mergetb/mcc/pkg/errors"
)

func TestNoRoutingPrefix(t *testing.T) {

	x := fromFile("test/data/check-no-routing-prefix.b64", t)

	c := &ValidAddresses{}
	ok, diags, err := c.Check(x)

	if ok {
		t.Fatal("failed to find address without routing prefix")
	}

	if len(*diags) != 1 {
		t.Fatal("unexpected number of diagnostics")
	}

	if !errors.Is(err, merr.ErrInvalidAddress) {
		t.Fatalf("Unexpected error: %v", err)
	}

	t.Logf("no routing prefix error: %v", err)
	t.Logf("no routing diagnostic: %s", (*diags)[0].Message)
}

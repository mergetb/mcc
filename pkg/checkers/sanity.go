package checkers

import (
	"fmt"
	"regexp"

	log "github.com/sirupsen/logrus"

	xir "gitlab.com/mergetb/xir/v0.3/go"

	merr "gitlab.com/mergetb/mcc/pkg/errors"
)

// Sanity checker. Checks for insanity.
// * models without nodes
// * sane node names
type Sanity struct {
}

const (
	rx = "^[a-zA-Z0-9]{1}[a-zA-Z0-9-]{0,62}$"
)

var (
	// I'm going with leading numerics are allowed.
	nodeName = regexp.MustCompile(rx)
)

// Name ...
func (s *Sanity) Name() string {
	return "sanity"
}

// Check the model for insanity.
func (s *Sanity) Check(x *xir.Network) (bool, *[]ModelDiagnostic, error) {

	log.Info("Sanity checking the model")

	var diags []ModelDiagnostic

	nodes := x.GetNodes()

	if len(nodes) == 0 {
		return false, nil, merr.ErrNoNodes
	}

	// invalid node names
	// https://gitlab.com/mergetb/mcc/-/issues/17
	for _, n := range nodes {
		matches := nodeName.FindAllString(n.Id, -1)
		if len(matches) != 1 {
			diags = append(diags, ModelDiagnostic{
				Message:    fmt.Sprintf("invalid node name %s. Must match %s.", n.Id, rx),
				Constraint: "invalid node name",
				Node:       n.Id,
			})
		}
	}

	if len(diags) > 0 {
		return false, &diags, merr.ErrInvalidNodeName
	}

	return true, nil, nil
}

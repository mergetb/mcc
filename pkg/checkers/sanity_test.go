package checkers

import (
	"testing"
)

func TestBadNodeName(t *testing.T) {

	x := fromFile("test/data/model_badname.b64", t)
	c := Sanity{}

	ok, ds, err := c.Check(x)
	if ok {
		t.Fatal(err)
	}

	t.Logf("bad names diags %v", ds)

	if len(*ds) != 2 {
		t.Fatal("expected only two bad node names")
	}
}

func TestEmptyModel(t *testing.T) {
	x := fromFile("test/data/empty.b64", t)
	c := Sanity{}

	ok, _, err := c.Check(x)
	if ok || err == nil {
		t.Fatal("expected to fail, should be empty")
	}
}

package checkers

import (
	xir "gitlab.com/mergetb/xir/v0.3/go"

	merr "gitlab.com/mergetb/mcc/pkg/errors"
)

type NodeSelfLink struct {
}

func (sl *NodeSelfLink) Name() string {
	return "node-self-link"
}

func (sl *NodeSelfLink) Check(xn *xir.Network) (bool, *[]ModelDiagnostic, error) {

	for _, l := range xn.Links {
		for i, ep1 := range l.Endpoints {
			for j, ep2 := range l.Endpoints {

				if i == j {
					continue
				}

				if ep1.Socket.Element == ep2.Socket.Element {

					mds := []ModelDiagnostic{{
						Message: "node self-link detected",
						Node:    ep1.Socket.Element,
						Link:    l.Id,
					}}

					return false, &mds, merr.ErrNodeSelfLink
				}
			}
		}
	}

	return true, nil, nil
}

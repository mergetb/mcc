package checkers

import (
	"fmt"
	"net"

	log "github.com/sirupsen/logrus"

	xir "gitlab.com/mergetb/xir/v0.3/go"

	merr "gitlab.com/mergetb/mcc/pkg/errors"
)

// Scan for network addresses in the model which are invalid.
type ValidAddresses struct {
}

func (va *ValidAddresses) Name() string {
	return "valid-addresses"
}

func (va *ValidAddresses) Check(x *xir.Network) (bool, *[]ModelDiagnostic, error) {

	log.Info("Checking for invalid addresses in the model")

	var diags []ModelDiagnostic

	for _, n := range x.GetNodes() {
		for _, s := range n.Sockets {
			for _, a := range s.Addrs {
				_, _, err := net.ParseCIDR(a)
				if err != nil {
					diags = append(diags, ModelDiagnostic{
						Message:    fmt.Sprintf("Address %s on node %s is not CIDR. A routing prefix (e.g. /24) is required on addresses.", a, n.Id),
						Constraint: "invalid address",
						Node:       n.Id,
					})
				}
			}
		}
	}

	if len(diags) > 0 {
		return false, &diags, merr.ErrInvalidAddress
	}

	return true, nil, nil
}

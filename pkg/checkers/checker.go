package checkers

import (
	"gitlab.com/mergetb/xir/v0.3/go"
)

// ModelDiagnostic contains information about model errors/problems.
type ModelDiagnostic struct {
	Message    string `json:"message,omitempty"`
	Constraint string `json:"constraint,omitempty"`
	Node       string `json:"node,omitempty"`
	Link       string `json:"link,omitempty"`
}

type ModelDiagnostics []*ModelDiagnostic

// ModelChecker interface takes a model and checks for problems, errors, etc.
// If there are problems found, it gives useful diagnostic information.
type ModelChecker interface {
	// Human readable name for the instance.
	Name() string
	// Check takes a model and returns true if the model is OK to be used.
	// Otherwise it returns false with diagnostics. Error on internal
	// or unexpected error.
	Check(x *xir.Network) (bool, *[]ModelDiagnostic, error)
}

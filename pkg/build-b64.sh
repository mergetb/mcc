#!/usr/bin/bash

PY2XIR=/usr/bin/py2xir.py

for f in "$@"; do
    if [[ $f == *.py ]] ; then
        cat $f | python3 $PY2XIR > ${f%.py}.b64
    fi
done

# special case, built by hand as we want addressing in the model
# command:
# cat large-routes.py | python3 /usr/bin/py2xir.py | ../../../build/mcc ipv4-addressing > large-routes.b64

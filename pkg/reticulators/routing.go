package reticulators

import (
	"fmt"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/xir/v0.3/go"
)

// StaticRouting ...
type StaticRouting struct {
}

// Name implements the Reticulator interface.
func (sr *StaticRouting) Name() string {
	return "static-routing"
}

// Reticulate the given XIR
func (sr *StaticRouting) Reticulate(xn *xir.Network) (bool, error) {

	if xn.Parameters == nil || xn.Parameters.Routing == nil || xn.Parameters.Routing.Value != xir.Routing_StaticRouting {
		log.Info("This model does not specify automatic static routing")
		return true, nil
	}
	log.Info("Adding ipv4 static routes to the model")

	err := DoBuildStaticRoutes(xn)
	if err != nil {
		return false, fmt.Errorf("Error adding static routes to the model: %w", err)
	}
	log.Info("Added routes as props on the network nodes")

	return true, nil
}

package reticulators

/*
	This doesn't produce good routes yet. It's fast though for approximations.
	Unfortunately, these approximations do not produce usable routes right now for a lot of topologies.
	The BFS one works by computing shortest paths to and from all nodes.

	The MST solution effectively approximates the BFS solution by instead of producing all shortest routes,
	produces all shortest routes from any node N to all other nodes and reverse routing from any node S to D via S->N->D.

	In theory, you can improve the approximations by doing more BFS searches, but that makes the routes unusable, as nodes need to "agree" on how to get to a node.
*/

import (
	"fmt"
	"strings"
	"sync"
	"time"

	"github.com/gammazero/workerpool"
	log "github.com/sirupsen/logrus"
	"gonum.org/v1/gonum/graph"
	"k8s.io/apimachinery/pkg/util/sets"

	xir "gitlab.com/mergetb/xir/v0.3/go"

	merr "gitlab.com/mergetb/mcc/pkg/errors"
)

func hopsLinksPathString(path ...graph.Node) string {

	hops := []string{}
	for _, h := range path {
		s := h.(*xir.LinkGraphVertex)
		hops = append(hops, s.String())
	}
	return strings.Join(hops, "|")
}

type mstEntry struct {
	lock  sync.Mutex
	route *mstRoute
}

type mstRoute struct {
	weight int
	src    *netNode
	via    *netNode
	dst    *netNode
	path   []graph.Node
}

type reverseRoute struct {
	root graph.Node
	src  *netNode
	via  *netNode
	ids  []int64
	dsts []*netNode
}

type mstTable struct {
	labelToSocket map[string]*xir.Socket
	socketToLink  map[*xir.Socket]*xir.Link
	linkHasAddr   map[*xir.Link]bool
	// spaths: actually a 2D array: src.ID, dst.ID -> route
	spaths [][]*mstEntry
	// rppaths: actually a map: src.ID -> list of parent reverse routes
	rppaths [][]*reverseRoute
	// rcpaths: actually a map: src.ID -> list of child reverse routes
	rcpaths   [][]*reverseRoute
	wp        *workerpool.WorkerPool
	linkGraph *xir.LinkGraph
	net       *xir.Network
}

func (t *mstTable) setRoutes(hops []graph.Node, reverse bool) (bool, error) {
	if t == nil || t.spaths == nil {
		return false, nil
	}

	src := hops[0]
	dst := hops[len(hops)-1]

	if reverse {
		dst = hops[0]
		src = hops[len(hops)-1]
	}

	be := t.spaths[src.ID()][dst.ID()]
	be.lock.Lock()
	defer be.lock.Unlock()

	// nowhere or the same
	if len(hops) <= 1 {
		t.spaths[src.ID()][dst.ID()].route = &mstRoute{
			weight: len(hops),
			path:   hops,
		}
		return false, nil
	}

	if be.route == nil || len(hops) < be.route.weight ||
		(len(hops) == be.route.weight && be.route.via == nil) {
		// log.Infof("adding %s", hopsLinksPathString(hops...))
		ans, err := t.computeNetNodes(hops, reverse)

		if err != nil {
			return false, nil
		}

		t.spaths[src.ID()][dst.ID()].route = ans
		return true, nil
	}

	if len(be.route.path) == 0 {
		be.route.path = hops
	}

	return false, nil
}

func (t *mstTable) initTable(net *xir.Network, workers int) {

	for _, node := range net.GetNodes() {
		for _, socket := range node.GetSockets() {
			label := socket.Label(node.Id)

			t.labelToSocket[label] = socket
		}
	}

	for _, link := range net.GetLinks() {
		var sockets []*xir.Socket

		foundAddr := true
		for _, ep := range link.Endpoints {
			socket := t.labelToSocket[ep.Socket.Label()]
			if len(socket.Addrs) == 0 {
				foundAddr = false
			}

			t.socketToLink[socket] = link

			sockets = append(sockets, socket)
		}

		t.linkHasAddr[link] = foundAddr
	}

	// debug only - dump as dot
	// b, _ := g.ToDot()
	// log.Debugf(string(b))

	num_nodes := t.linkGraph.Nodes().Len()

	t.spaths = make([][]*mstEntry, num_nodes)
	t.rppaths = make([][]*reverseRoute, num_nodes)
	t.rcpaths = make([][]*reverseRoute, num_nodes)
	for i := 0; i < num_nodes; i++ {
		t.spaths[i] = make([]*mstEntry, num_nodes)

		for j := 0; j < num_nodes; j++ {
			t.spaths[i][j] = &mstEntry{}
		}
	}
}

// DoBuildStaticRoutes ...
func doBuildStaticRoutesMST(net *xir.Network, workers int) error {

	g, _ := net.ToLinkGraph(false)

	t := &mstTable{
		labelToSocket: make(map[string]*xir.Socket),
		socketToLink:  make(map[*xir.Socket]*xir.Link),
		linkHasAddr:   make(map[*xir.Link]bool),
		net:           net,
		linkGraph:     g,
		wp:            workerpool.New(workers),
		// will make paths later
	}

	defer t.wp.Stop()

	t.initTable(net, workers)

	//
	log.Infof("computing routes via mst...")
	comp_start := time.Now()

	finishedNodes := make(sets.Int64)
	var allNodes []int64
	for _, n := range net.GetLinks() {
		gn := t.linkGraph.NodeByName(n.Id)
		if gn == nil {
			continue
		}

		allNodes = append(allNodes, gn.ID())

		t.spaths[gn.ID()][gn.ID()].route = &mstRoute{
			weight: 1,
		}

		// add a shortest path to each neighbor
		to := g.From(gn.ID())

		for to.Next() {
			next := to.Node()
			nid := next.ID()

			t.spaths[gn.ID()][nid].route = &mstRoute{
				weight: 2,
			}
			t.spaths[nid][gn.ID()].route = &mstRoute{
				weight: 2,
			}
		}
	}

	for _, n := range net.GetNodes() {
		gn := t.linkGraph.NodeByName(n.Id)
		if gn == nil {
			continue
		}

		allNodes = append(allNodes, gn.ID())

		t.spaths[gn.ID()][gn.ID()].route = &mstRoute{
			weight: 1,
		}

		// add a shortest path to each neighbor
		to := g.From(gn.ID())

		for to.Next() {
			next := to.Node()
			nid := next.ID()

			t.spaths[gn.ID()][nid].route = &mstRoute{
				weight: 2,
			}
			t.spaths[nid][gn.ID()].route = &mstRoute{
				weight: 2,
			}
		}
	}

	var wg sync.WaitGroup
	err_ch := make(chan error, workers)
	defer close(err_ch)

	num_nodes := t.linkGraph.Nodes().Len()
	consideredNodes := allNodes
	cyclesFinishedNodes := make(sets.Int64)
	anyCycle := false

	allNodesSet := make(sets.Int64)
	allNodesSet.Insert(allNodes...)

	for !finishedNodes.HasAll(allNodes...) {
		maxdegree := -1
		var src_n graph.Node
		// find out the node/link with the most amount of edges,
		// as we do not want a terminal node (a node with only 1 edge) as our root,
		// so that all of the final nodes that it reached are terminals
		// note that if they're all terminals, then it's fine,
		// as the graph would only contain pairs/islands
		for _, id := range consideredNodes {
			if finishedNodes.Has(id) {
				continue
			}

			// node/link has to have ip addresses on everything
			gn := t.linkGraph.Node(id)
			if gn == nil {
				continue
			}

			v, ok := gn.(*xir.LinkGraphVertex)

			if !ok {
				continue
			}

			// every link that the node is in should have an ip addr
			// this doesn't work for some reason
			if v.IsNode() {
				continue
				// for _, socket := range v.Node.Sockets {
				// 	link := t.socketToLink[socket]

				// 	if !t.linkHasAddr[link] {
				// 		continue
				// 	}
				// }
			}

			// is link without addr
			if !v.IsNode() && !t.linkHasAddr[v.Link] {
				continue
			}

			to := t.linkGraph.From(id)

			degree := 0
			for to.Next() {
				degree++
			}

			if degree > maxdegree {
				src_n = t.linkGraph.Node(id)
				maxdegree = degree
			}
		}

		// construct our current mst edges
		// this is actually a map of src/dst -> bool
		mstEdges := make([][]bool, num_nodes)
		for i := range mstEdges {
			mstEdges[i] = make([]bool, num_nodes)
		}

		// construct our child nodes
		// this is actually a map of src -> children
		childNodes := make([][]int64, num_nodes)

		// the starting link will partition the graph into multiple sections,
		// these are the partitions
		partition_ids := make(map[int64][]int64)
		partition_dst := make(map[int64][]*netNode)
		var parts []graph.Node

		bfs := BreadthFirst{
			Visit: func(path []graph.Node) {
				// log.Infof("found path: %s", hopsLinksPathString(path...))

				if len(path) == 2 {
					partition_ids[path[1].ID()] = nil
					partition_dst[path[1].ID()] = nil
					parts = append(parts, path[1])
				}

				// add this as a valid edge to our mst
				if len(path) >= 2 {
					next := path[len(path)-1].ID()
					prev := path[len(path)-2].ID()

					mstEdges[next][prev] = true
					mstEdges[prev][next] = true

					childNodes[prev] = append(childNodes[prev], next)
				}

				pcopy := make([]graph.Node, len(path))
				copy(pcopy, path)

				_, err := t.setRoutes(pcopy, false)
				if err != nil {
					log.Error(err)
				}

				_, err = t.setRoutes(pcopy, true)
				if err != nil {
					log.Error(err)
				}
			},

			Traverse: func(e graph.Edge) bool {
				return !finishedNodes.Has(e.To().ID()) &&
					!finishedNodes.Has(e.From().ID())
			},
		}

		//log.Infof("Start BFS")
		found_nodes, terminals, g_cycles := bfs.Walk(t.linkGraph, src_n)
		cycles := make([]graph.Node, 0, len(g_cycles))
		for _, c := range g_cycles {
			v, ok := c.(*xir.LinkGraphVertex)

			if !ok || v.IsNode() || cyclesFinishedNodes.Has(c.ID()) {
				continue
			}

			cycles = append(cycles, c)
		}

		log.Infof("End BFS: %s, cycles: %d", src_n, len(cycles))
		// log.Infof("terminals: %s", terminals)

		anyCycle = anyCycle || len(cycles) > 0

		// figure out all found destinations
		all_dsts := []*netNode{}
		all_ids := []int64{}

		found_nodes.Iter(func(d int64, _ struct{}) (stop bool) {
			if t.spaths[src_n.ID()][d].route != nil &&
				t.spaths[src_n.ID()][d].route.dst != nil {
				all_dsts = append(all_dsts, t.spaths[src_n.ID()][d].route.dst)
				all_ids = append(all_ids, d)
			}

			return false
		})

		// actually a map of id to if we've seen it or not
		found_children := make([]bool, num_nodes)

		tbfs := BreadthFirst{
			Visit: func(path []graph.Node) {
				// log.Info("")
				log.Infof("tbfs: found path: %s", hopsLinksPathString(path...))

				cur := path[len(path)-1]
				// log.Infof("cur: %s", cur)

				found_children[cur.ID()] = true

				proute := t.spaths[cur.ID()][src_n.ID()].route
				log.Infof("tbfs: found parent path: %s", hopsLinksPathString(proute.path...))

				// add the global reverse route if this isn't the root of a partition
				rroute := &reverseRoute{
					root: src_n,
					src:  proute.src,
					via:  proute.via,
					dsts: all_dsts,
					ids:  all_ids,
				}

				// add the parent/root reverse route
				t.rppaths[cur.ID()] = append(t.rppaths[cur.ID()], rroute)

				if len(proute.path) > 1 {
					// figure out what partition this node belongs to
					partition := proute.path[1].ID()
					if partition == src_n.ID() {
						partition = cur.ID()
					}

					if partition != cur.ID() &&
						t.spaths[src_n.ID()][cur.ID()].route != nil &&
						t.spaths[src_n.ID()][cur.ID()].route.dst != nil {

						partition_ids[partition] = append(partition_ids[partition], cur.ID())
						partition_dst[partition] = append(partition_dst[partition], t.spaths[src_n.ID()][cur.ID()].route.dst)
					}

					// add child routes to this node's parent
					parent := proute.path[len(proute.path)-2]
					// log.Infof("parent: %s -> %s", parent, cur)

					route, err := t.computeNetNodes([]graph.Node{parent, cur}, false)
					if err != nil {
						log.Error(err)
						return
					}

					if len(path) > 2 {
						prev := path[len(path)-2].(*xir.LinkGraphVertex)

						proute, err := t.computeNetNodes([]graph.Node{parent, cur, prev}, false)
						if err != nil {
							log.Error(err)
							return
						}

						route.via = proute.via
					}

					merged_cr := &reverseRoute{
						root: src_n,
						src:  route.src,
						via:  route.via,
					}

					for _, cr := range t.rcpaths[cur.ID()] {
						merged_cr.dsts = append(merged_cr.dsts, cr.dsts...)
						merged_cr.ids = append(merged_cr.ids, cr.ids...)
					}

					if route.dst != nil {
						merged_cr.dsts = append(merged_cr.dsts, route.dst)
						merged_cr.ids = append(merged_cr.ids, cur.ID())
					}

					t.rcpaths[parent.ID()] = append(t.rcpaths[parent.ID()], merged_cr)
				}

			},

			HoldVisit: func(n graph.Node) bool {
				for _, child := range childNodes[n.ID()] {
					if !found_children[child] {
						return true
					}
				}

				return false
			},

			Traverse: func(e graph.Edge) bool {
				to := e.To().ID()
				from := e.From().ID()

				return mstEdges[to][from] && // in our mst
					to != src_n.ID() && from != src_n.ID() && // don't have bfs cross the root node
					!finishedNodes.Has(to) &&
					!finishedNodes.Has(from)
			},
		}

		// don't record anything because we already know what it is
		tbfs.Walk(t.linkGraph, terminals...)

		// we now need to fix the routing for each node adjacent to the source
		for _, p1 := range parts {
			// we need to fix both of these for adjacent nodes
			t.rcpaths[p1.ID()] = nil
			t.rppaths[p1.ID()] = nil

			for _, p2 := range parts {

				// this is our parition, so we have to compute via for each destination properly
				if p1 == p2 {
					for _, dest := range partition_ids[p2.ID()] {
						src_route := t.spaths[src_n.ID()][dest]
						path := src_route.route.path[1:]

						// this is a shortest route, as src -> p1 -> dest is a shortest path,
						// so p1 -> dest and dest -> p1 is also a shorest path
						_, err := t.setRoutes(path, false)
						if err != nil {
							log.Error(err)
							continue
						}

						_, err = t.setRoutes(path, true)
						if err != nil {
							log.Error(err)
							continue
						}
					}

				}

				// otherwise, just add all of the routes via the other partition root
				path := []graph.Node{p1, src_n, p2}
				proute, err := t.computeNetNodes(path, false)
				if err != nil {
					log.Error(err)
					continue
				}

				rroute := &reverseRoute{
					root: src_n,
					src:  proute.src,
					via:  proute.via,
					dsts: partition_dst[p2.ID()],
					ids:  partition_ids[p2.ID()],
				}

				// add the parent/root reverse route
				t.rppaths[p1.ID()] = append(t.rppaths[p1.ID()], rroute)
			}
		}

		// move child and parent paths into spaths
		// in order to do this, we have to calculate the lengths of the paths,
		// so we skip doing this if we don't need it
		if anyCycle {
			cur_time := time.Now()
			for i := range t.spaths {
				i := i
				id := int64(i)

				if id == src_n.ID() {
					continue
				}

				wg.Add(1)

				f := func() {
					defer wg.Done()

					reverseRoutes := append(t.rcpaths[i], t.rppaths[i]...)

					for _, croute := range reverseRoutes {
						if croute == nil {
							continue
						}

						for j, dst := range croute.dsts {
							if dst == nil {
								continue
							}

							d_id := croute.ids[j]

							if i == int(d_id) {
								continue
							}

							path1 := t.spaths[src_n.ID()][id].route.path
							path2 := t.spaths[src_n.ID()][d_id].route.path

							weight := 0
							shared := 1
							// same partition
							if path1[1].ID() == path2[1].ID() {

								shared = 2
								for shared < len(path1)-1 && shared < len(path2)-1 && path1[shared].ID() == path2[shared].ID() {
									shared++
								}

								weight = len(path1) + len(path2) + 1 - (2 * shared)
								// weight = len(path1[shared:]) + len(path2[shared:]) + 1

							} else {
								// different partition,
								weight = t.spaths[src_n.ID()][id].route.weight +
									t.spaths[src_n.ID()][d_id].route.weight - 1
							}

							// log.Infof("")
							// log.Infof("cur: %s", t.linkGraph.Node(id))
							// log.Infof("p1: %s", t.spaths[src_n.ID()][id].route.path)
							// log.Infof("p2: %s", t.spaths[src_n.ID()][d_id].route.path)
							// log.Infof("paritions identical: %t, weight: %d, shared: %d", path1[1] == path2[1], weight, shared)

							p := &mstRoute{
								src:    croute.src,
								via:    croute.via,
								dst:    dst,
								weight: weight,
							}

							be := t.spaths[id][d_id]
							be.lock.Lock()

							if be.route == nil || weight < be.route.weight {
								t.spaths[id][d_id].route = p
							}

							be.lock.Unlock()
						}
					}
				}

				if workers == 1 {
					f()
				} else {
					t.wp.Submit(f)
				}
			}

			wg.Wait()

			log.Infof("moved child and parent paths into shortest paths, took: %s", time.Since(cur_time))
		}

		// delete all paths, we don't need them anymore
		for i := range t.spaths {
			for j := range t.spaths[i] {
				if t.spaths[i][j].route != nil {
					t.spaths[i][j].route.path = nil
				}
			}
		}

		// delete parent and child stuff
		if anyCycle {
			for i := range t.rppaths {
				t.rppaths[i] = nil
				t.rcpaths[i] = nil
			}
		}

		if len(cycles) > 0 {
			cyclesFinishedNodes = cyclesFinishedNodes.Insert(src_n.ID())

			consideredNodes = nil
			for _, term := range terminals {
				for _, c := range cycles {
					if c == term && !cyclesFinishedNodes.Has(term.ID()) {
						consideredNodes = append(consideredNodes, term.ID())
					}
				}
			}

			if len(consideredNodes) == 0 {
				log.Infof("cycles were not terminals")
				for _, term := range terminals {
					if !cyclesFinishedNodes.Has(term.ID()) {
						consideredNodes = append(consideredNodes, term.ID())
					}
				}
			}

			// I don't think there are any more nodes
			if len(consideredNodes) == 0 {
				found_nodes.Iter(
					func(d int64, _ struct{}) (stop bool) {
						finishedNodes.Insert(d)
						return false
					},
				)
				finishedNodes = finishedNodes.Union(cyclesFinishedNodes)

				cyclesFinishedNodes = make(sets.Int64)

				left := allNodesSet.Difference(finishedNodes)
				consideredNodes = left.UnsortedList()
			}
		} else {
			found_nodes.Iter(
				func(d int64, _ struct{}) (stop bool) {
					finishedNodes.Insert(d)
					return false
				},
			)

			finishedNodes = finishedNodes.Union(cyclesFinishedNodes)

			cyclesFinishedNodes = make(sets.Int64)

			left := allNodesSet.Difference(finishedNodes)
			consideredNodes = left.UnsortedList()
		}

	}

	log.Debugf("computing took: %s, writing routes...", time.Since(comp_start))
	writing_start := time.Now()

	// create routes from the shortest paths

	for _, src := range net.GetNodes() {
		src := src

		if workers == 1 {
			err := t.getLinkRoutes(src)
			if err != nil {
				return err
			}
		} else {
			t.wp.Submit(func() {
				err := t.getLinkRoutes(src)
				err_ch <- err
			})
		}

	}

	if workers > 1 {
		for range net.GetNodes() {
			err := <-err_ch
			if err != nil {
				return err
			}
		}
	}

	log.Debugf("writing took: %s", time.Since(writing_start))

	return nil
}

func (t *mstTable) computeNetNodes(ghops []graph.Node, reverse bool) (*mstRoute, error) {
	var hops []*xir.LinkGraphVertex
	weight := len(ghops)

	for _, h := range ghops {
		v, ok := h.(*xir.LinkGraphVertex)
		if !ok {
			return nil, fmt.Errorf("bad vertex type in hops")
		}

		hops = append(hops, v)
	}

	// if hops == 0, not found
	// if hops == 1, it's itself
	// we don't need to actually compute these
	if len(hops) <= 1 {
		return &mstRoute{
			weight: weight,
			path:   ghops,
		}, nil
	}

	// log.Debugf("route dump %s", linkPathString(hops))

	if !reverse {
		ultimate_node := hops[len(hops)-1]
		penultimate_node := hops[len(hops)-2]

		dlink := ultimate_node.Link
		dnode := penultimate_node.Node
		if ultimate_node.IsNode() {
			dnode = ultimate_node.Node
			dlink = penultimate_node.Link
		}

		dst_nn, err := t.getNetNode(dnode, dlink)
		if err != nil {
			log.Warnf("cannot convert dst to net node: %s", err)
			return nil, merr.ErrInvalidNetwork
		}

		var src_nn *netNode
		if hops[0].IsNode() {
			src_nn, err = t.getNetNode(hops[0].Node, hops[1].Link)
			if err != nil {
				log.Debugf("cannot convert src to net node: %s", err)
				return nil, merr.ErrInvalidNetwork
			}
		}

		var via_nn *netNode
		if dst_nn != nil {
			fhops := hops
			if src_nn != nil {
				fhops = hops[1:]
			}

			if len(fhops) == 1 {
				fhops = []*xir.LinkGraphVertex{hops[1], hops[0]}
			}

			via_nn, err = t.computeGateway(fhops, false)
			if err != nil {
				return nil, err
			}

			if via_nn == nil {
				log.Infof("via was nil: %s", linkPathString(fhops))
			}
		}

		return &mstRoute{
			weight: weight,
			src:    src_nn,
			dst:    dst_nn,
			via:    via_nn,
			path:   ghops,
		}, nil
	}

	ultimate_node := hops[0]
	penultimate_node := hops[1]

	dlink := ultimate_node.Link
	dnode := penultimate_node.Node
	if ultimate_node.IsNode() {
		dnode = ultimate_node.Node
		dlink = penultimate_node.Link
	}

	dst_nn, err := t.getNetNode(dnode, dlink)
	if err != nil {
		log.Warnf("cannot convert dst to net node: %s", err)
		return nil, merr.ErrInvalidNetwork
	}

	var src_nn *netNode
	if hops[len(hops)-1].IsNode() {
		src_nn, err = t.getNetNode(hops[len(hops)-1].Node, hops[len(hops)-2].Link)
		if err != nil {
			log.Debugf("cannot convert src to net node: %s", err)
			return nil, merr.ErrInvalidNetwork
		}
	}

	var via_nn *netNode
	if dst_nn != nil {
		fhops := hops
		if src_nn != nil {
			fhops = hops[:len(hops)-1]
		}

		if len(fhops) == 1 {
			fhops = []*xir.LinkGraphVertex{hops[len(hops)-1], hops[len(hops)-2]}
		}

		via_nn, err = t.computeGateway(fhops, true)
		if err != nil && dst_nn != nil {
			return nil, err
		}

		if via_nn == nil {
			log.Infof("via was nil: %s", linkPathString(fhops))
		}
	}

	return &mstRoute{
		weight: weight,
		src:    src_nn,
		dst:    dst_nn,
		via:    via_nn,
		path:   ghops,
	}, nil

}

// first thing must be a link
func (t *mstTable) computeGateway(hops []*xir.LinkGraphVertex, reverse bool) (*netNode, error) {
	// walk the next hops looking for our gw. gw is first addressable node we find.
	// We may be next to a layer 2 node so we skip those until we find a node with an address.
	var gw *netNode
	var err error

	// if len(hops) <= 2 {
	// 	log.Infof("computeGateway: %s", linkPathString(hops))
	// }

	for i := 0; i < len(hops)-1; i += 2 {
		link := hops[i].Link
		node := hops[i+1].Node

		if reverse {
			j := len(hops) - 1 - i
			link = hops[j].Link
			node = hops[j-1].Node
		}

		gw, err = t.getNetNode(node, link)

		if err != nil {
			log.Debugf("cannot convert gw candidate %s to net node: %s",
				node,
				err,
			)

			continue
		}

		if gw == nil { // not a layer 3 node
			log.Debugf("Skipping non-layer 3 node %s", node.Id)
			continue
		} else {
			log.Debugf("Found gw: %s", gw)
			break
		}
	}

	if gw == nil {
		log.Infof("Unable to find valid gw node for path %t: %s", reverse, linkPathString(hops))
		return nil, nil
	}

	return gw, nil
}

func (t *mstTable) getLinkRoutes(src_n *xir.Node) error {
	// We keep track of routes by src & gw so we can remove redundant routes later.
	type routeKey struct {
		Src string
		Gw  string
	}

	src_g := t.linkGraph.NodeByName(src_n.Id)
	// not in the graph, so it only has 1 socket
	var src *netNode
	var err error
	var link *xir.Link
	if src_g == nil {
		// no sockets, not in graph, so do nothing
		if len(src_n.Sockets) < 1 {
			return nil
		}
		if len(src_n.Sockets) > 1 {
			return fmt.Errorf("somehow not in graph, but more than 1 socket: %s", src_n.Id)
		}

		link = t.socketToLink[src_n.Sockets[0]]
		src_g = t.linkGraph.NodeByName(link.Id)

		if src_g == nil {
			return fmt.Errorf("somehow, link not in graph: %s", link.Id)
		}

		src, err = t.getNetNode(src_n, link)
		if err != nil {
			log.Errorf("cannot convert src to net node: %s", err)
			return merr.ErrInvalidNetwork
		}
	}

	destinations := make(sets.Set[string])

	// log.Debugf("final routes from %s:", src_n.GetId())

	if src_n.Conf == nil {
		src_n.Conf = &xir.NodeConfig{
			Routes: []*xir.RouteConfig{},
		}
	}

	addMstRoute := func(p *mstRoute, trueRoute bool) error {

		// no routes, so continue
		if p == nil {
			// log.Infof("p is nil!")
			return nil
		}

		src := src
		if p.src != nil {
			src = p.src
		}
		dst := p.dst
		gw := p.via

		if src == nil {
			log.Debugf("Skipping non-layer 3 node %s", src)
			return nil
		}

		if dst == nil {
			log.Debugf("Skipping non-layer 3 node %s", dst)
			return nil
		}

		dst_s := dst.SNet

		if p.weight <= 1 && trueRoute {
			// adjacent, don't bother
			destinations.Insert(dst_s)
			return nil
		}

		// the dst could be the gw, which will happen if all of the intermediate hops are layer-2 only
		neigh, err := nodesShareSubnet(src, dst)
		if err != nil {
			log.Error(err)
			return merr.ErrInvalidNetwork
		}

		if neigh {
			log.Debugf("nodes are layer-3 neighbors; use link-local route")
			destinations.Insert(dst_s)
			return nil
		}

		// log.Debugf("Path shows non-neighbors, adding route for %s|%s -> %s|%s via %s|%s",
		// 	src.Node.Id,
		// 	src.IP,
		// 	dst.Node.Id,
		// 	dst.Net,
		// 	gw.Node.Id,
		// 	gw.IP,
		// )

		if !destinations.Has(dst_s) {
			// log.Debugf("\t%s -> %s ----> %s", src.Net, gw.IP, dst.Net)

			destinations.Insert(dst_s)

			src_n.Conf.Routes = append(src_n.Conf.Routes, &xir.RouteConfig{
				Src: src.SNet,
				Gw:  gw.SIP,
				Dst: dst_s,
			})
		}

		return nil
	}

	// log.Infof("true routes")
	for dst := range t.spaths[src_g.ID()] {
		p := t.spaths[src_g.ID()][dst].route

		err := addMstRoute(p, true)
		if err != nil {
			return err
		}
	}

	// log.Infof("inferred routes from children: len: %d", len(t.rcpaths[src_g.ID()]))
	reverseRoutes := append(t.rcpaths[src_g.ID()], t.rppaths[src_g.ID()]...)
	for _, croute := range reverseRoutes {
		if croute == nil {
			continue
		}

		// if i == len(reverseRoutes)-1 {
		// 	log.Infof("inferred routes from parent")
		// }

		p := &mstRoute{
			src: croute.src,
			via: croute.via,
		}

		// log.Infof("parent dsts: %+v", croute.dsts)

		for _, dst := range croute.dsts {
			if dst == nil {
				continue
			}

			p.dst = dst

			err := addMstRoute(p, false)
			if err != nil {
				return err
			}

		}
	}

	return nil
}

func (t *mstTable) getNetNode(node *xir.Node, link *xir.Link) (*netNode, error) {

	if link == nil || node == nil {
		return nil, fmt.Errorf("link or node is nil, cannot compute")
	}

	var socket *xir.Socket
	for _, s := range node.Sockets {
		if t.socketToLink[s] == link {
			socket = s
			break
		}
	}

	if socket == nil {
		return nil, fmt.Errorf("could not find a socket for node %s in link %s", node.Id, link.Id)
	}

	log.Debugf("link: %s", link)
	if layer3Supported(link) == false {
		if socket.Addrs == nil {
			// in a layer 2 link and has no address means this is a layer 2 interface.
			return nil, nil
		}
		// If there is an address, then this is a layer 3 interface in a layer 2 link, which
		// is fine and expected so just continue on.
	}

	n, err := socket2IPNetFirst(socket, nil)
	if err != nil {
		// layer 3 but no address.
		return nil, fmt.Errorf("%w: %s", merr.ErrNoAddressOnEndpoint, socket)
	}

	n.Node = node

	return &n, nil
}

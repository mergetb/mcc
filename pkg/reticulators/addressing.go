package reticulators

import (
	"fmt"
	"net"
	"strings"

	log "github.com/sirupsen/logrus"
	xir "gitlab.com/mergetb/xir/v0.3/go"

	merr "gitlab.com/mergetb/mcc/pkg/errors"
)

// IPv4Addressing holds data and implements the Reticulator interface
type IPv4Addressing struct {

	// AddrSpace keeps track of the "next" unused subnet.
	AddrSpace *net.IPNet

	// ActiveNets keeps track of all subnets currently in use.
	ActiveNets map[string]bool
}

// NewIPv4Addressing returns an initialized addressing reticulator.
func NewIPv4Addressing() *IPv4Addressing {
	_, ipn, _ := net.ParseCIDR("10.0.0.0/24")
	return &IPv4Addressing{
		AddrSpace:  ipn,
		ActiveNets: make(map[string]bool),
	}
}

// Name is a Reticulator interface function.
func (a *IPv4Addressing) Name() string {
	return "ipv4-addressing"
}

// Reticulate is a Reticulator interface function.
//
// The basic strategy is to add addresses to each link. If a link
// has existing addresses, add addresses from the link's subnet
// that are not in use. If a link has no addresses, assign it an
// unused subnet (starting at 10.0.X.0) and fill it with addresses
// from the unused subnet.
//
// Assumptions:
//   - /24 subnet for each link.
//   - existing addresses on a link are in the same subnet.
func (a *IPv4Addressing) Reticulate(xn *xir.Network) (bool, error) {

	if xn.Parameters == nil || xn.Parameters.Addressing == nil || xn.Parameters.Addressing.Value != xir.Addressing_IPv4Addressing {
		log.Info("This model does not require automatiic ipv4 addressing")
		return true, nil
	}
	log.Info("Adding ipv4 addressing to the model")

	err := a.buildActiveSubnets(xn)
	if err != nil {
		return false, err
	}

	for _, l := range xn.GetLinks() {

		if layer3Supported(l) == false {
			log.Debugf("Igoring non-layer 3 link %s", l.GetId())
			continue
		}

		// Read existing addresses.
		linkAddrs := []*net.IPNet{}
		linkNets := []*net.IPNet{}

		epAddrs := make(map[*xir.Endpoint]net.IP)

		for _, ep := range l.Endpoints {

			n := xn.GetNode(ep.Socket.Element)

			ip, ipn, err := a.socket2IPNet(n.Sockets[ep.Socket.Index])
			if err != nil {
				return false, err
			}
			if ipn == nil {
				continue
			}

			// check for inconsistent subnets.
			// i.e. if two addresses on different endpoints in the link
			// are not in the same subnet, complain.
			for _, ln := range linkNets {
				if !ln.Contains(ip) {
					return false, merr.ErrMultipleLinkSubnets
				}
			}

			linkAddrs = append(linkAddrs, &net.IPNet{IP: ip, Mask: ipn.Mask})
			linkNets = append(linkNets, ipn)
			epAddrs[ep] = ip
		}

		if len(linkAddrs) == 0 {
			log.Debugf("No addresses in link %s. Add new subnet and addresses.", l.Id)
		} else {
			log.Debugf(
				"%d of %d endpoints in link %s have addresses. Use exising subnet.",
				len(linkAddrs), len(l.Endpoints), l.Id,
			)
		}

		// Now fill in any missing addresses, possibly all. Loop over all endpoints
		// in the link, adding unused addresses to all endpoints without addresses.
		for _, ep := range l.Endpoints {
			if _, ok := epAddrs[ep]; !ok {

				n := xn.GetNode(ep.Socket.Element)
				if n == nil {
					return false, fmt.Errorf("Node not found: %s", ep.Socket.Element)
				}

				// no addr for this endpoint, add it.
				epa, err := a.nextAddr(&linkAddrs)
				if err != nil {
					return false, err
				}

				log.Debugf("Adding addr %s to socket %s", epa.String(), ep.Socket.Label())

				if len(n.Sockets) < int(ep.Socket.Index) {
					return false, fmt.Errorf("Inconsistent link in model. vs. node sockets in model.")
				}

				n.Sockets[ep.Socket.Index].Addrs = append(n.Sockets[ep.Socket.Index].Addrs, epa.String())
			}
		}

	}
	return true, nil
}

// buildActiveSubnets - given a model, build entries in the ActiveNets. i.e. find all subnets
// in use in the xir and store them in a.ActiveNets. Also check for duplicate subnets
// across all links.
func (a *IPv4Addressing) buildActiveSubnets(net *xir.Network) error {

	// check for duplicate subnets across all links.
	seenSubnets := make(map[string]string)
	l2Links := layer2Links(net)

	for _, l := range net.GetLinks() {

		l2Connected := layer2Connected(l, l2Links)

		for _, ep := range l.Endpoints {

			node := net.GetNode(ep.Socket.Element)
			socket := node.Sockets[ep.Socket.Index] // out of bounds check? shouldn't happen but...

			_, ipn, err := a.socket2IPNet(socket)
			if err != nil {
				return err
			}

			if ipn != nil {

				subnet := ipn.String()

				log.Debugf("Marking subnet as in use %s", subnet)
				a.ActiveNets[subnet] = true

				// if this link does not have a layer 2 interface, then check for duplicate subnets.
				// Otherwise duplicate subnets across this link may be OK. We cannot determine that from
				// here so we need to trust the model developer.
				if !l2Connected {
					if ln, ok := seenSubnets[subnet]; ok {
						if ln != l.Id {
							return fmt.Errorf(
								"%w: Found same subnet %s in distinct links %s and %s ",
								merr.ErrDuplicateSubnets,
								subnet,
								ln,
								l.Id,
							)
						}
					} else {
						log.Debugf("Marking subnet %s as seen in link %s", subnet, l.Id)
						seenSubnets[subnet] = l.Id
					}
				}
			}
		}
	}

	log.Debugf("existing subnets: %+v", a.ActiveNets)

	return nil
}

// nextAddr returns the next address that doesn't exist in the
// given addresses. If the given addresses are empty, it will
// find the next (globally) unused subnet and return an address from that.
func (a *IPv4Addressing) nextAddr(addrs *[]*net.IPNet) (*net.IPNet, error) {

	// Find the "next" address that is not already in the list.
	var result *net.IPNet

	log.Debugf("current link addrs: %+v", *addrs)

	if len(*addrs) == 0 {
		net, err := a.nextSubnet()
		if err != nil {
			return nil, err
		}

		result = net

	} else {
		result = a.copyIPNet((*addrs)[0])
	}

	activeAddrs := make(map[string]bool)
	for _, a := range *addrs {
		activeAddrs[a.String()] = true
	}

	lastQuad := len(result.IP) - 1
	result.IP[lastQuad] = 0 // start with least in subnet

	// find next available address
	result.IP[lastQuad]++
	for {
		_, ok := activeAddrs[result.String()]
		if !ok {
			break
		}
		result.IP[lastQuad]++
		if result.IP[lastQuad] > 254 {
			return nil,
				fmt.Errorf("Out of /24 address space for space %s", result)
		}
	}

	*addrs = append(*addrs, result)

	return result, nil
}

// nextSubnet returns an unused/unassigned /24 subnet.
func (a *IPv4Addressing) nextSubnet() (*net.IPNet, error) {

	// Find next unused subnet.
	a.incrementAddressSpace(a.AddrSpace.IP)
	for {
		_, ok := a.ActiveNets[a.AddrSpace.String()]
		if !ok {
			break
		}
		a.incrementAddressSpace(a.AddrSpace.IP)
	}

	log.Debugf("New subnet %s", a.AddrSpace.String())
	a.ActiveNets[a.AddrSpace.String()] = true

	result := a.copyIPNet(a.AddrSpace)
	return result, nil
}

func (a *IPv4Addressing) copyIPNet(ipn *net.IPNet) *net.IPNet {

	copyBytes := func(in []byte) []byte {
		out := make([]byte, len(in))
		for i, b := range in {
			out[i] = b
		}
		return out
	}

	return &net.IPNet{IP: copyBytes(ipn.IP), Mask: copyBytes(ipn.Mask)}
}

// incrementAddressSpace increments an IP address space starting at quad 2.
func (a *IPv4Addressing) incrementAddressSpace(ip net.IP) {

	for i := len(ip) - 2; i >= 0; i-- {
		ip[i]++
		if ip[i] > 0 {
			break
		}
	}
}

// socket2IPNet converts the IP addresses on the endpoint to a golang IP and IPNet.
// Returns nils if the endpoint has no address or is unable to convert. Returns
// error if the address exists, but cannot be parsed or has an error.
func (a *IPv4Addressing) socket2IPNet(s *xir.Socket) (net.IP, *net.IPNet, error) {

	// Just skip sockets without addresses.
	if len(s.Addrs) == 0 {
		return nil, nil, nil
	}

	if len(s.Addrs) > 1 {
		log.Warnf("Using first address of socket addresses %s. Ignoring the others.",
			strings.Join(s.Addrs, ", "))
	}

	// TODO: handle multi-addressed sockets.
	// this is not great, assume the first address is the one we want.
	ip, ipn, err := net.ParseCIDR(s.Addrs[0])
	if err != nil {
		log.Warnf("Bad CIDR for endpoint: %+v", err)
		return nil, nil, fmt.Errorf("Bad CIDR for endpoint: %w", err)
	}

	log.Debugf("Existing addr: %s (%s) for %s", ip, ipn, s.Endpoint.Element)

	return ip, ipn, nil
}

func layer3Supported(l *xir.Link) bool {

	if l == nil || l.Layer == nil {
		return true
	}

	// check if this constraint allows layer 3
	c := l.Layer
	switch c.Op {
	case xir.Operator_EQ:
		return c.Value == 3 // "layer == 2" is not layer 3, but "layer == 3" is.
	case xir.Operator_NE:
		return c.Value != 3 // "layer != 3" is not layer 3, but "layer != 2" is
	case xir.Operator_LT:
		return c.Value >= 3 // "layer < 3" is not layer 3, but "layer < 4" is.
	}
	return false
}

// Return list of links that do not support layer 3 in the giuen network
func layer2Links(net *xir.Network) []*xir.Link {

	l2links := []*xir.Link{}

	for _, l := range net.GetLinks() {
		if !layer3Supported(l) {
			l2links = append(l2links, l)
		}
	}

	return l2links
}

// Return true if the given link shares a node with any of the layer 2
// links given.
func layer2Connected(l *xir.Link, l2Links []*xir.Link) bool {

	if len(l2Links) == 0 {
		return false
	}

	for _, ep := range l.Endpoints {
		for _, l2l := range l2Links {
			for _, l2ep := range l2l.Endpoints {
				if ep.Socket.Element == l2ep.Socket.Element {
					// these links share a node - they are connected
					return true
				}
			}
		}
	}

	return false
}

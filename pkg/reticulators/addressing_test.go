package reticulators

import (
	"errors"
	"io/ioutil"
	"net"
	"testing"

	xir "gitlab.com/mergetb/xir/v0.3/go"

	merr "gitlab.com/mergetb/mcc/pkg/errors"
)

func fromFile(path string, t *testing.T) *xir.Network {

	buf, err := ioutil.ReadFile(path)
	if err != nil {
		t.Fatal(err)
	}

	net, err := xir.NetworkFromB64String(string(buf))
	if err != nil {
		t.Fatal(err)
	}

	return net
}

func getAddressReticulated(f string, t *testing.T) *xir.Network {

	x := fromFile(f, t)
	a := NewIPv4Addressing()
	ok, err := a.Reticulate(x)
	if err != nil {
		t.Fatal(err)
	}
	if !ok {
		t.Fatal("Addressing reticulator failed")
	}

	return x
}

func TestAddressCoverage(t *testing.T) {

	files := []string{
		"test/data/addressing-noaddr.b64",
		"test/data/addressing-someaddr.b64",
	}

	for _, f := range files {

		x := getAddressReticulated(f, t)

		// Just confirm that all endpoints have addresses
		for _, node := range x.GetNodes() {
			for _, s := range node.Sockets {
				if len(s.Addrs) == 0 {
					t.Fatalf(
						"node %s socket missing address: %s",
						node.GetId(), s.Endpoint.Label(),
					)
				}
			}
		}
	}
}

func TestNoDupAddresses(t *testing.T) {

	files := []string{
		"test/data/addressing-noaddr.b64",
		"test/data/addressing-someaddr.b64",
	}

	for _, f := range files {

		x := getAddressReticulated(f, t)

		seen := make(map[string]string)

		// Just confirm that all endpoints have addresses
		for _, node := range x.GetNodes() {
			for _, s := range node.Sockets {
				for _, a := range s.Addrs {
					if _, ok := seen[a]; ok {
						t.Fatalf(
							"duplicate address %s in %s and %s",
							a, seen[a], s.Endpoint.Label(),
						)
					}
					seen[a] = s.Endpoint.Label()
				}
			}
		}
	}
}

func TestTooManyEndpoints(t *testing.T) {

	x := fromFile("test/data/addressing-lotsaendpoints.b64", t)
	a := NewIPv4Addressing()
	ok, err := a.Reticulate(x)
	// we expect an error here.
	if err == nil {
		t.Fatal(err)
	}
	if ok {
		t.Fatal("expected not ok w/too many endpoints in a link")
	}
}

func TestLotsaLinks(t *testing.T) {

	// Given > 255 links we expect subnets from 10.0.0.0 => 10.X.Y.0
	// This file has 260 links, so we expect subnets
	// from 10.0.0.0 => 10.1.3.0
	x := getAddressReticulated("test/data/addressing-lotsalinks.b64", t)

	seen := make(map[string]string)

	// Just confirm that all sockets have addresses
	for _, node := range x.GetNodes() {
		for _, s := range node.Sockets {
			for _, a := range s.Addrs {
				seen[a] = s.Endpoint.Label()
			}
		}
	}

	expected := []string{
		"10.0.1.1/24",
		"10.0.132.1/24",
		"10.0.255.1/24",
		"10.1.3.1/24",
		"10.1.4.1/24",
	}

	notexpected := []string{
		"10.0.0.1/24", // we skip this one
		"10.1.5.1/24",
	}

	for _, addr := range expected {
		if _, ok := seen[addr]; !ok {
			t.Fatalf("Missing expected address %s", addr)
		}
	}

	for _, addr := range notexpected {
		if _, ok := seen[addr]; ok {
			t.Fatalf("Unexpected address exists: %s", addr)
		}
	}

}

func TestSubnetAddressing(t *testing.T) {

	// Here is the model under test:
	//
	// # The a net is not multilink.
	// a = [net.device('a%d'%i) for i in range(3)]
	// net.connect([a[0], a[1]])
	// a_net = net.connect([a[0], a[2]])
	// a_net[a[2]].ip.addrs = ['10.0.1.47/24']
	//
	// # the b net is multilink
	// b = [net.device('b%d'%i) for i in range(3)]
	// net_b = net.connect(b)
	// net_b[b[0]].ip.addrs = ['172.0.47.1/24']
	//
	// # the c net is multilink w/no addresses
	// c = [net.device('c%d'%i) for i in range(3)]
	//
	// s = net.device('s')
	//
	// s_a = net.connect([s, a[0]])
	// s_b = net.connect([s, b[0]])
	// net.connect([s, c])
	//
	// s_a[s].ip.addrs = ['10.0.10.1/24']
	// s_b[s].ip.addrs = ['10.0.20.1/24']

	// Given this:
	//		a0 should have a 10.0.1.47 endpoint
	//      a1 should have an unspecifed but unique subnet
	//      the bX nodes should have a 172.0.47.0 subnet endpoint
	//      the cX nodes should have an unspecifed but unique subnet
	//      s should have at least a 10.0.1.0 and 172.0.47.0 address.

	x := getAddressReticulated("test/data/addressing-someaddr.b64", t)

	type testData struct {
		Net   string
		Nodes []string
	}

	td := []testData{{
		Net:   "10.0.1.0/24",
		Nodes: []string{"a0", "a2"},
	}, {
		Net:   "172.0.47.0/24",
		Nodes: []string{"b0", "b1", "b2"},
	}, {
		Net:   "10.0.10.0/24",
		Nodes: []string{"s", "a0"},
	}, {
		Net:   "10.0.20.0/24",
		Nodes: []string{"s", "b0"},
	},
	}

	for _, d := range td {
		_, subnet, err := net.ParseCIDR(d.Net)
		if err != nil {
			t.Fatal(err)
		}

		for _, id := range d.Nodes {

			node := x.GetNode(id)
			if node == nil {
				t.Fatalf("node missing: %s", id)
			}

			inSubnet := false
			for _, s := range node.Sockets {

				if len(s.Addrs) == 0 {
					t.Fatalf("node %s ep %s fail: %s", id, s.Endpoint.Label(), err)
				}

				for _, a := range s.Addrs {

					t.Logf("node %s ep addr %s", id, a)
					ip, _, err := net.ParseCIDR(a)
					if err != nil {
						t.Fatal(err)
					}

					if subnet.Contains(ip) == true {
						inSubnet = true
						break
					}
				}
			}
			if inSubnet == false {
				t.Fatalf("node %s has no endpoints in subnet %s", id, subnet)
			}
		}
	}
}

func TestLayer2NonAddressing(t *testing.T) {

	x := getAddressReticulated("test/data/addressing-somelayer2.b64", t)

	// Node 'a0' has three sockets, the last of which is in a Link at layer 2.
	// So it should not have an address. The others should.

	n := x.GetNode("a0")
	if len(n.Sockets) != 3 {
		t.Fatalf("unxpected number of sockets")
	}

	if len(n.Sockets[0].Addrs) != 1 || len(n.Sockets[1].Addrs) != 1 {
		t.Fatalf("Missing address on node socket")
	}

	if len(n.Sockets[2].Addrs) > 0 {
		t.Fatalf("Address on socket in layer 2 link")
	}
}

// Make sure we generate an error when there are duplicate subnets in different links.
func TestDuplicateSubnets(t *testing.T) {

	x := fromFile("test/data/addressing-duplicate-subnet.b64", t)
	a := NewIPv4Addressing()
	_, err := a.Reticulate(x)

	if err == nil {
		t.Fatalf("expected to find duplicate subnets error")
	}

	if !errors.Is(err, merr.ErrDuplicateSubnets) {
		t.Fatalf("failed to find duplicate subnet")
	}
}

// duplicate subnets are OK when the link has a layer2 socket.
func TestDuplicateSubnetsLayer2(t *testing.T) {

	x := fromFile("test/data/addressing-duplicate-subnet-layer2.b64", t)
	a := NewIPv4Addressing()
	_, err := a.Reticulate(x)

	if err != nil {
		t.Fatalf("duplicate subnets error: %+v", err)
	}
}

func TestJelenasModel(t *testing.T) {

	x := fromFile("test/data/addressing-jelena.b64", t)
	a := NewIPv4Addressing()
	_, err := a.Reticulate(x)

	if err != nil {
		t.Fatalf("duplicate subnets error: %+v", err)
	}
}

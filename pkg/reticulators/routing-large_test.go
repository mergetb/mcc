package reticulators

import (
	"testing"
)

func TestStaticRoutingLarge(t *testing.T) {

	net := readRoutingXirT("./test/data/large-routes.b64", t)
	err := DoBuildStaticRoutes(net)
	if err != nil {
		t.Fatal(err)
	}
}

func TestStaticRouting10000Web(t *testing.T) {

	net := readRoutingXirT("./test/data/10648-nodes.b64", t)
	err := DoBuildStaticRoutes(net)
	if err != nil {
		t.Fatal(err)
	}
}

func TestStaticRouting100000Web(t *testing.T) {

	net := readRoutingXirT("./test/data/103823-nodes.b64", t)
	err := DoBuildStaticRoutes(net)
	if err != nil {
		t.Fatal(err)
	}
}

func TestStaticRouting10Ring(t *testing.T) {

	net := readRoutingXirT("./test/data/10-ring.b64", t)
	err := DoBuildStaticRoutes(net)
	if err != nil {
		t.Fatal(err)
	}
}

func TestStaticRouting1kRing(t *testing.T) {

	net := readRoutingXirT("./test/data/1k-ring.b64", t)
	err := DoBuildStaticRoutes(net)
	if err != nil {
		t.Fatal(err)
	}
}

func testStaticRouting10kRing(t *testing.T) {

	net := readRoutingXirT("./test/data/10k-ring.b64", t)
	err := DoBuildStaticRoutes(net)
	if err != nil {
		t.Fatal(err)
	}
}

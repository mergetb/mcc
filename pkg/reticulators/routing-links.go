package reticulators

import (
	"fmt"
	"net/netip"
	"strings"
	"sync"
	"time"

	"github.com/dolthub/swiss"
	"github.com/gammazero/workerpool"
	"github.com/puzpuzpuz/xsync/v3"
	log "github.com/sirupsen/logrus"
	"gonum.org/v1/gonum/graph"

	xir "gitlab.com/mergetb/xir/v0.3/go"

	merr "gitlab.com/mergetb/mcc/pkg/errors"
)

type linkEntry struct {
	lock  sync.Mutex
	route *linkRoute
}

type linkRoute struct {
	weight int
	src    *netNode
	via    *netNode
	dst    *netNode
}

type linkTable struct {
	labelToSocket *swiss.Map[string, *xir.Socket]
	socketToLink  *swiss.Map[*xir.Socket, *xir.Link]
	// paths: src.ID, dst.ID -> route
	paths     [][]*linkEntry
	wp        *workerpool.WorkerPool
	linkGraph *xir.LinkGraph
	net       *xir.Network
	cidrCache *xsync.MapOf[string, netNode]
}

func linkPathString(path []*xir.LinkGraphVertex) string {

	hops := []string{}
	for _, h := range path {
		hops = append(hops, h.String())
	}
	return strings.Join(hops, "|")
}

func (t *linkTable) setRoutes(hops []graph.Node, reverse bool) (bool, error) {
	if t == nil || t.paths == nil {
		return false, nil
	}

	src := hops[0]
	dst := hops[len(hops)-1]

	if reverse {
		dst = hops[0]
		src = hops[len(hops)-1]
	}

	be := t.paths[src.ID()][dst.ID()]
	be.lock.Lock()
	defer be.lock.Unlock()

	// nowhere, the same, or adjacent
	if len(hops) <= 2 {
		t.paths[src.ID()][dst.ID()].route = &linkRoute{
			weight: len(hops),
		}
		return false, nil
	}

	if be.route == nil || len(hops) < be.route.weight {
		log.Debugf("adding %s -> %s", src, dst)
		ans, err := t.computeNetNodes(hops, reverse)

		if err != nil {
			return false, nil
		}

		t.paths[src.ID()][dst.ID()].route = ans
		return true, nil
	}

	return false, nil
}

// DoBuildStaticRoutes ...
func doBuildStaticRoutesLinks(net *xir.Network, workers int) error {

	g, _ := net.ToLinkGraph(false)

	num_sockets := 0
	for _, node := range net.GetNodes() {
		num_sockets += len(node.GetSockets())
	}

	t := &linkTable{
		labelToSocket: swiss.NewMap[string, *xir.Socket](uint32(num_sockets)),
		socketToLink:  swiss.NewMap[*xir.Socket, *xir.Link](uint32(num_sockets)),
		net:           net,
		linkGraph:     g,
		wp:            workerpool.New(workers),
		cidrCache:     xsync.NewMapOfPresized[string, netNode](num_sockets),
		// will make paths later
	}

	defer t.wp.Stop()

	for _, node := range net.GetNodes() {
		for _, socket := range node.GetSockets() {
			label := socket.Label(node.Id)

			t.labelToSocket.Put(label, socket)
		}
	}

	for _, link := range net.GetLinks() {
		var sockets []*xir.Socket

		for _, ep := range link.Endpoints {
			socket, _ := t.labelToSocket.Get(ep.Socket.Label())

			t.socketToLink.Put(socket, link)

			sockets = append(sockets, socket)
		}
	}

	// debug only - dump as dot
	// b, _ := g.ToDot()
	// log.Debugf(string(b))

	err_ch := make(chan error, workers)
	defer close(err_ch)

	num_nodes := g.Nodes().Len()

	t.paths = make([][]*linkEntry, num_nodes)
	for i := 0; i < num_nodes; i++ {
		t.paths[i] = make([]*linkEntry, num_nodes)
		for j := 0; j < num_nodes; j++ {
			t.paths[i][j] = &linkEntry{}
		}
	}

	log.Infof("computing pairs via links...")
	comp_start := time.Now()

	var wg sync.WaitGroup

	for _, l := range net.GetLinks() {
		ln := t.linkGraph.NodeByName(l.Id)
		if ln == nil {
			continue
		}

		wg.Add(1)

		f := func() {
			defer wg.Done()
			bfs := BreadthFirst{
				Visit: func(path []graph.Node) {
					//log.Debugf("found path: %s", hopsLinksPathString(path...))
					_, err := t.setRoutes(path, false)
					if err != nil {
						log.Error(err)
					}

					_, err = t.setRoutes(path, true)
					if err != nil {
						log.Error(err)
					}
				},
			}

			bfs.Walk(t.linkGraph, ln)
		}

		if workers == 1 {
			f()
		} else {
			t.wp.Submit(f)
		}
	}

	if workers > 1 {
		wg.Wait()
	}

	log.Debugf("computing took: %s, writing routes...", time.Since(comp_start))
	writing_start := time.Now()

	// create routes from the shortest paths

	for _, src := range net.GetNodes() {
		src := src

		if workers == 1 {
			err := t.getLinkRoutes(src)
			if err != nil {
				return err
			}
		} else {
			t.wp.Submit(func() {
				err := t.getLinkRoutes(src)
				err_ch <- err
			})
		}

	}

	if workers > 1 {
		for range net.GetNodes() {
			err := <-err_ch
			if err != nil {
				return err
			}
		}
	}

	log.Debugf("writing took: %s", time.Since(writing_start))

	return nil
}

func (t *linkTable) computeNetNodes(ghops []graph.Node, reverse bool) (*linkRoute, error) {
	weight := len(ghops)

	// if hops == 0, not found
	// if hops == 1, it's itself
	// if hops == 2, it's adjacent
	// we don't need to actually compute these
	if len(ghops) <= 2 {
		return &linkRoute{
			weight: weight,
		}, nil
	}

	//log.Debugf("route dump %s", linkPathString(hops))

	// yeah, converting everything to *xir.LinkGraphVertex is ugly,
	// but making a new slice and "new" objects is really, really slow

	if !reverse {
		ultimate_node := ghops[len(ghops)-1]
		penultimate_node := ghops[len(ghops)-2]

		dlink := ultimate_node.(*xir.LinkGraphVertex).Link
		dnode := penultimate_node.(*xir.LinkGraphVertex).Node
		if ultimate_node.(*xir.LinkGraphVertex).IsNode() {
			dnode = ultimate_node.(*xir.LinkGraphVertex).Node
			dlink = penultimate_node.(*xir.LinkGraphVertex).Link
		}

		dst_nn, err := t.getNetNode(dnode, dlink)
		if err != nil {
			log.Warnf("cannot convert dst to net node: %s", err)
			return nil, err
		}

		var src_nn *netNode
		if ghops[0].(*xir.LinkGraphVertex).IsNode() {
			src_nn, err = t.getNetNode(ghops[0].(*xir.LinkGraphVertex).Node, ghops[1].(*xir.LinkGraphVertex).Link)
			if err != nil {
				log.Debugf("cannot convert src to net node: %s", err)
				return nil, err
			}
		}

		var via_nn *netNode
		if dst_nn != nil {
			fhops := ghops
			if src_nn != nil {
				fhops = ghops[1:]
			}

			via_nn, err = t.computeGateway(fhops, false)
			if err != nil {
				return nil, err
			}
		}

		return &linkRoute{
			weight: weight,
			src:    src_nn,
			dst:    dst_nn,
			via:    via_nn,
		}, nil
	}

	ultimate_node := ghops[0]
	penultimate_node := ghops[1]

	dlink := ultimate_node.(*xir.LinkGraphVertex).Link
	dnode := penultimate_node.(*xir.LinkGraphVertex).Node
	if ultimate_node.(*xir.LinkGraphVertex).IsNode() {
		dnode = ultimate_node.(*xir.LinkGraphVertex).Node
		dlink = penultimate_node.(*xir.LinkGraphVertex).Link
	}

	dst_nn, err := t.getNetNode(dnode, dlink)
	if err != nil {
		log.Warnf("cannot convert dst to net node: %s", err)
		return nil, err
	}

	var src_nn *netNode
	if ghops[len(ghops)-1].(*xir.LinkGraphVertex).IsNode() {
		src_nn, err = t.getNetNode(ghops[len(ghops)-1].(*xir.LinkGraphVertex).Node, ghops[len(ghops)-2].(*xir.LinkGraphVertex).Link)
		if err != nil {
			log.Debugf("cannot convert src to net node: %s", err)
			return nil, fmt.Errorf("cannot convert src to net node: %w", err)
		}
	}

	var via_nn *netNode
	if dst_nn != nil {
		fhops := ghops
		if src_nn != nil {
			fhops = ghops[:len(ghops)-1]
		}

		via_nn, err = t.computeGateway(fhops, true)
		if err != nil && dst_nn != nil {
			return nil, err
		}
	}

	return &linkRoute{
		weight: weight,
		src:    src_nn,
		dst:    dst_nn,
		via:    via_nn,
	}, nil

}

// first thing must be a link
func (t *linkTable) computeGateway(hops []graph.Node, reverse bool) (*netNode, error) {
	// walk the next hops looking for our gw. gw is first addressable node we find.
	// We may be next to a layer 2 node so we skip those until we find a node with an address.
	var gw *netNode
	var err error

	for i := 0; i < len(hops)-1; i += 2 {
		link := hops[i].(*xir.LinkGraphVertex).Link
		node := hops[i+1].(*xir.LinkGraphVertex).Node

		if reverse {
			j := len(hops) - 1 - i
			link = hops[j].(*xir.LinkGraphVertex).Link
			node = hops[j-1].(*xir.LinkGraphVertex).Node
		}

		gw, err = t.getNetNode(node, link)

		if err == nil && gw == nil { // not a layer 3 node
			log.Debugf("Skipping non-layer 3 node %s", node.Id)
			continue
		}

		if err != nil {
			log.Debugf("cannot convert gw candidate %s to net node: %s",
				node,
				err,
			)

			continue
		}

		if gw != nil {
			log.Debugf("Found gw: %s", gw)
			break
		}
	}

	if gw == nil {
		//log.Debugf("Unable to find valid gw node for path %s", linkPathString(hops))
		return nil, nil
	}

	return gw, nil
}

func (t *linkTable) getLinkRoutes(src_n *xir.Node) error {
	// We keep track of routes by src & gw so we can remove redundant routes later.
	type routeKey struct {
		Src string
		Gw  string
	}

	src_g := t.linkGraph.NodeByName(src_n.Id)
	// not in the graph, so it only has 1 socket
	var src *netNode
	var err error
	if src_g == nil {
		// no sockets, not in graph, so do nothing
		if len(src_n.Sockets) < 1 {
			return nil
		}
		if len(src_n.Sockets) > 1 {
			return fmt.Errorf("somehow not in graph, but more than 1 socket: %s", src_n.Id)
		}

		link, _ := t.socketToLink.Get(src_n.Sockets[0])
		src_g = t.linkGraph.NodeByName(link.Id)

		if src_g == nil {
			return fmt.Errorf("somehow, link not in graph: %s", link.Id)
		}

		src, err = t.getNetNode(src_n, link)
		if err != nil {
			log.Errorf("cannot convert src to net node: %s", err)
			return err
		}
	}

	// log.Debugf("final routes from %s:", src_n.GetId())

	size := len(t.paths[src_g.ID()])

	destinations := swiss.NewMap[netip.Prefix, struct{}](uint32(size))

	if src_n.Conf == nil {
		src_n.Conf = &xir.NodeConfig{
			Routes: make([]*xir.RouteConfig, 0, size),
		}
	}

	for dst := range t.paths[src_g.ID()] {

		p := t.paths[src_g.ID()][dst].route

		// no routes, so continue
		if p == nil {
			continue
		}

		if p.src != nil {
			src = p.src
		}
		dst := p.dst
		gw := p.via

		if src == nil {
			log.Debugf("Skipping non-layer 3 node %s", src)
			continue
		}

		if dst == nil {
			log.Debugf("Skipping non-layer 3 node %s", dst)
			continue
		}

		// log.Debugf("Path shows non-neighbors, adding route for %s|%s -> %s|%s via %s|%s",
		// 	src.Node.Id,
		// 	src.IP,
		// 	dst.Node.Id,
		// 	dst.Net,
		// 	gw.Node.Id,
		// 	gw.IP,
		// )

		if !destinations.Has(dst.Net) {
			// log.Infof("\t%s -> %s ----> %s", src.Net.String(), gw.IP.String(), dst.Net.String())

			destinations.Put(dst.Net, struct{}{})

			if src.Net.Overlaps(dst.Net) {
				log.Debugf("nodes are layer-3 neighbors; use link-local route")
				continue
			}

			src_n.Conf.Routes = append(src_n.Conf.Routes, &xir.RouteConfig{
				Src: src.SNet,
				Gw:  gw.SIP,
				Dst: dst.SNet,
			})
		}

	}

	return nil
}

func (t *linkTable) getNetNode(node *xir.Node, link *xir.Link) (*netNode, error) {

	if link == nil || node == nil {
		return nil, fmt.Errorf("link or node is nil, cannot compute")
	}

	var socket *xir.Socket
	for _, s := range node.Sockets {
		o, _ := t.socketToLink.Get(s)
		if o == link {
			socket = s
			break
		}
	}

	if socket == nil {
		return nil, fmt.Errorf("could not find a socket for node %s in link %s", node.Id, link.Id)
	}

	log.Debugf("link: %s", link)
	if layer3Supported(link) == false {
		if socket.Addrs == nil {
			// in a layer 2 link and has no address means this is a layer 2 interface.
			return nil, nil
		}
		// If there is an address, then this is a layer 3 interface in a layer 2 link, which
		// is fine and expected so just continue on.
	}

	n, err := socket2IPNetFirst(socket, t.cidrCache)
	if err != nil {
		// layer 3 but no address.
		return nil, fmt.Errorf("%w: %s", merr.ErrNoAddressOnEndpoint, socket)
	}

	n.Node = node

	return &n, nil
}

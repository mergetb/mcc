package reticulators

// based on https://github.com/gonum/gonum/blob/v0.13.0/graph/traverse/traverse.go

import (
	// log "github.com/sirupsen/logrus"
	"github.com/dolthub/swiss"
	"github.com/edwingeng/deque/v2"
	"gonum.org/v1/gonum/graph"
)

type Path struct {
	node graph.Node
	path []graph.Node
}

type BreadthFirst struct {
	// Visit is called on all nodes on their first visit.
	// If you want a copy of the path, please copy it yourself,
	// as it may share a pointer with other paths
	Visit func([]graph.Node)

	HoldVisit func(graph.Node) bool

	// Traverse is called on all edges that may be traversed
	// during the walk. This includes edges that would hop to
	// an already visited node.
	//
	// The value returned by Traverse determines whether
	// an edge can be traversed during the walk.
	Traverse func(graph.Edge) bool

	queue   *deque.Deque[Path]
	visited *swiss.Map[int64, struct{}]
}

// Walk performs a breadth-first traversal of the graph g starting from the given node,
// depending on the Traverse field.
// The traversal follows edges for which Traverse(edge) is true.
// This returns all the visited nodes, all terminal nodes, and the nodes that detected a cycle
func (b *BreadthFirst) Walk(g graph.Graph, froms ...graph.Node) (*swiss.Map[int64, struct{}], []graph.Node, []graph.Node) {
	if b.visited == nil {
		b.visited = swiss.NewMap[int64, struct{}](uint32(g.Nodes().Len()))
	}

	if b.queue == nil {
		b.queue = deque.NewDeque[Path]()
	}

	for _, from := range froms {
		parents := []graph.Node{from}

		b.queue.PushBack(Path{from, parents})
		b.visited.Put(from.ID(), struct{}{})
	}

	var terminals []graph.Node
	var cycles []graph.Node

	for b.queue.Len() > 0 {
		path := b.queue.PopFront()
		t := path.node
		parents := path.path

		if b.HoldVisit != nil && b.HoldVisit(t) {
			b.queue.PushBack(path)
			continue
		}

		if b.Visit != nil {
			b.Visit(parents)
		}

		tid := t.ID()

		// get number of valid children
		children := 0
		to := g.From(tid)
		for to.Next() {
			n := to.Node()
			nid := n.ID()

			if b.Traverse != nil && !b.Traverse(g.Edge(tid, nid)) {
				continue
			}

			if b.visited.Has(nid) {
				// log.Infof("previously visited: %s via %s", n, hopsLinksPathString(parents...))
				// the last node is itself, so look at the one before it
				if len(parents) > 1 {
					old_nid := parents[len(parents)-2].ID()
					if old_nid != nid {
						cycles = append(cycles, t)
					}
				}

				continue
			}

			children++
		}

		if children == 0 {
			terminals = append(terminals, t)
			continue
		}

		to = g.From(tid)
		for to.Next() {
			n := to.Node()
			nid := n.ID()
			if b.Traverse != nil && !b.Traverse(g.Edge(tid, nid)) {
				continue
			}
			if b.visited.Has(nid) {
				continue
			}

			var new_parents []graph.Node
			if children > 1 {
				// we will need to copy over the array, otherwise the children will share a pointer
				new_parents = make([]graph.Node, len(parents)+1)
				copy(new_parents, parents)
				new_parents[len(parents)] = n
			} else {
				new_parents = append(parents, n)
			}

			b.visited.Put(nid, struct{}{})

			b.queue.PushBack(Path{
				node: n,
				path: new_parents,
			})
		}
	}

	return b.visited, terminals, cycles
}

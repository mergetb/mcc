package reticulators

import (
	"fmt"
	"strings"
	"time"

	"github.com/gammazero/workerpool"
	log "github.com/sirupsen/logrus"
	graph "gonum.org/v1/gonum/graph"
	"gonum.org/v1/gonum/graph/path"

	xir "gitlab.com/mergetb/xir/v0.3/go"

	merr "gitlab.com/mergetb/mcc/pkg/errors"
)

func socketPathString(path []graph.Node) string {

	hops := []string{}
	for _, h := range path {
		s := h.(*xir.SocketGraphVertex)
		hops = append(hops, s.String())
	}
	return strings.Join(hops, "|")
}

// DoBuildStaticRoutes ...
func doBuildStaticRoutesSockets(net *xir.Network, workers int) error {

	g, _ := net.ToSocketGraph()

	// debug only - dump as dot
	// b, _ := g.ToDot()
	// log.Debugf(string(b))

	wp := workerpool.New(workers)
	defer wp.Stop()

	err_ch := make(chan error, workers)
	defer close(err_ch)

	log.Infof("computing pairs via sockets...")
	comp_start := time.Now()

	// create routes from the shortest paths

	// nn == "xir.Network.Node", gn == "graph node"
	for _, nn0 := range net.GetNodes() {
		nn0 := nn0

		if workers == 1 {
			err := getSocketRoutes(net, g, nn0)
			if err != nil {
				return err
			}
		} else {
			wp.Submit(func() {
				err := getSocketRoutes(net, g, nn0)
				err_ch <- err
			})
		}
	}

	if workers > 1 {
		for range net.GetNodes() {
			err := <-err_ch
			if err != nil {
				return err
			}
		}
	}

	log.Debugf("computing took: %s", time.Since(comp_start))

	return nil
}

func getSocketRoutes(net *xir.Network, g *xir.SocketGraph, nn0 *xir.Node) error {
	// We keep track of routes by src & gw so we can remove redundant routes later.
	type routeKey struct {
		Src string
		Gw  string
	}

	p := path.DijkstraFrom(g.NodeByName(nn0.Id), g)

	routes := make(map[routeKey][]route)

	for _, nn1 := range net.GetNodes() {

		// don't route between a node and itself
		if nn0.GetId() == nn1.GetId() {
			continue
		}

		// We have to route to all sockets of the second node to get a full address space
		// as there may be a socket/subnet that does not have a direct path to nn0. (In the
		// case of asymetric routing, for instance.)

		for _, s := range nn1.GetSockets() {

			gs1 := g.NodeByName(s.Label(nn1.GetId()))

			// If there is an address, then this is a layer 3 interface in a layer 2 link, which
			// is fine and expected so just continue on.
			if layer3Supported(gs1.Link) == false && gs1.Socket.Addrs == nil {
				log.Debugf("skip routing for non-layer-3 link: %s", gs1.Link.GetId())
				continue
			}

			hops, _ := p.To(gs1.ID())

			// hops starts with a node and ends with the socket just before the node.
			// e.g. a -> a.0 -> .... -> b0.0 (-> b)
			// no path is ok, can happen if topology has islands
			// path with insufficient hops is not ok, suggests something is broken
			if len(hops) == 0 {
				log.Debugf("no path from %s to %s", nn0.Id, nn1.Id)
				continue
			} else if len(hops) < 3 {
				//log.Errorf("invalid path from %s to %s: %s (too few hops %d)", nn0.Id, nn1.Id, socketPathString(hops), len(hops))
				return merr.ErrInvalidNetwork
			}

			//log.Debugf("Shortest path (%d): %+v", len(hops), socketPathString(hops))

			// convert sockets into IP/IpNET hops. We only care about src, gw, and dst.
			src, err := socketHop2NetNode(hops[1])
			if err != nil {
				log.Debugf("cannot convert src to net node: %s", err)
				return err
			}

			if err == nil && src == nil {
				log.Debugf("Skipping non-layer 3 socket %s", hops[1].(*xir.SocketGraphVertex).String())
				continue
			}

			// walk the next hops looking for our gw. gw is first addressable node we find.
			// We may be next to a layer 2 node so we skip those until we find a node with an address.
			var gw *netNode
			for _, h := range hops[2:] {

				gw, err = socketHop2NetNode(h)

				if err == nil && gw == nil { // not a layer 3 node
					log.Debugf("Skipping non-layer 3 socket %s", h.(*xir.SocketGraphVertex).String())
					continue
				}

				if err != nil {
					log.Debugf("cannot convert gw candidate %s to net node: %s",
						h.(*xir.SocketGraphVertex).String(),
						err,
					)

					continue
				}

				if gw != nil {
					log.Debugf("Found gw: %s", gw)
					break
				}
			}

			if gw == nil {
				log.Errorf("Unable to find valid gw node for path %s", socketPathString(hops))
				return merr.ErrInvalidNetwork
			}

			dst, err := socketHop2NetNode(hops[len(hops)-1])
			if err != nil {
				return err
			}

			// the dst could be the gw, which will happen if all of the intermediate hops are layer-2 only
			neigh, err := nodesShareSubnet(src, dst)
			if err != nil {
				log.Error(err)
				return merr.ErrInvalidNetwork
			}

			if neigh {
				log.Debugf("nodes are layer-3 neighbors; use link-local route")
				continue
			}

			log.Debugf("Path shows non-neighbors, adding route for %s -> %s", nn0.GetId(), nn1.GetId())

			// Now we have all the src, gw, and dst we can add the route.
			r := route{
				Src: *src,
				Gw:  *gw,
				Dst: *dst,
			}
			log.Debugf("adding provisional route: %s", r)

			k := routeKey{Src: r.Src.Node.GetId(), Gw: r.Gw.Node.GetId()}
			routes[k] = append(routes[k], r)
		}
	}

	// we have all routes between s1 and s2, finalize the routes.
	finalRoutes := []route{}

	// Now for each set of routes per src/gw, we look for unneeded routes.
	// First pass is to throw out duplicate dst networks. Later pass is to
	// find the widest networks in overlaps.
	for _, rts := range routes {
		seen := make(map[string]bool)
		for _, r1 := range rts {
			if _, ok := seen[r1.Dst.Net.String()]; !ok {
				seen[r1.Dst.Net.String()] = true
				log.Debugf("adding final route: %s", r1)
				finalRoutes = append(finalRoutes, r1)
			} else {
				//log.Debugf("discarding %s, already seen", r1.Dst.Net.String())
			}
		}
	}

	// log.Debugf("final routes from %s:", nn0.GetId())
	// for _, r := range finalRoutes {
	// 	log.Debugf("\t%s -> %s ----> %s", r.Src.Node.GetId(), r.Gw.IP.String(), r.Dst.Net.String())
	// }

	if len(finalRoutes) != 0 {
		if nn0.Conf == nil {
			nn0.Conf = &xir.NodeConfig{
				Routes: []*xir.RouteConfig{},
			}
		}
		for _, fr := range finalRoutes {
			nn0.Conf.Routes = append(nn0.Conf.Routes, &xir.RouteConfig{
				Src: fr.Src.Net.String(),
				Gw:  fr.Gw.IP.String(),
				Dst: fr.Dst.Net.String(),
			})
		}
	}

	return nil
}

func socketHop2NetNode(hop graph.Node) (*netNode, error) {

	hs, ok := hop.(*xir.SocketGraphVertex)
	if !ok {
		return nil, fmt.Errorf("bad vertex type in socket graph")
	}

	if hs.IsNode() == true {
		return nil, fmt.Errorf("graph node %s has no socket", hs)
	}

	log.Debugf("link: %s", hs.Link)
	if layer3Supported(hs.Link) == false {
		if hs.Socket.Addrs == nil {
			// in a layer 2 link and has no address means this is a layer 2 interface.
			return nil, nil
		}
		// If there is an address, then this is a layer 3 interface in a layer 2 link, which
		// is fine and expected so just continue on.
	}

	n, err := socket2IPNetFirst(hs.Socket, nil)
	if err != nil {
		// layer 3 but no address.
		return nil, fmt.Errorf("%w: %s", merr.ErrNoAddressOnEndpoint, hs.Socket)
	}

	n.Node = hs.Node

	return &n, nil
}

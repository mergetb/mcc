package reticulators

import (
	"gitlab.com/mergetb/xir/v0.3/go"
)

// ModelReticulator takes a model and mutates it in someway.
type ModelReticulator interface {
	Name() string
	Reticulate(x *xir.Network) (bool, error)
}

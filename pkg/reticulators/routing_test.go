package reticulators

import (
	"io/ioutil"
	"runtime"
	"testing"

	log "github.com/sirupsen/logrus"
	xir "gitlab.com/mergetb/xir/v0.3/go"
)

func readRoutingXirT(path string, t *testing.T) *xir.Network {

	buf, err := ioutil.ReadFile(path)
	if err != nil {
		t.Fatalf("file read error %+v", err)
	}

	net, err := xir.NetworkFromB64String(string(buf))
	if err != nil {
		t.Fatal(err)
	}

	return net
}

func readRoutingXirB(path string, b *testing.B) *xir.Network {

	buf, err := ioutil.ReadFile(path)
	if err != nil {
		b.Fatalf("file read error %+v", err)
	}

	net, err := xir.NetworkFromB64String(string(buf))
	if err != nil {
		b.Fatal(err)
	}

	return net
}

type proproute struct {
	dst string
	gw  string
}

func readPropRoutes(node string, net *xir.Network, t *testing.T) []proproute {

	r := []proproute{}

	n := net.GetNode(node)
	if n == nil {
		t.Fatalf("no such node")
	}

	for _, rte := range n.Conf.Routes {
		r = append(r, proproute{dst: rte.Dst, gw: rte.Gw})
	}

	return r
}

// TestDoBuildStaticRoutes - Just test the ability to add any routes at all.
func TestStaticRoutingDoBuildStatic(t *testing.T) {

	// log.SetLevel(log.TraceLevel)

	net := readRoutingXirT("./test/data/simple-routes.b64", t)

	err := DoBuildStaticRoutes(net)
	if err != nil {
		t.Fatal(err)
	}

	// Assert that the node props have routes.
	if len(net.Nodes[0].Conf.Routes) == 0 {
		t.Fatalf("no route added")
	}
	t.Logf("node[0] routes: %+v", net.Nodes[0].Conf.Routes)
}

func TestStaticRoutingSpecific(t *testing.T) {

	// log.SetLevel(log.DebugLevel)

	type route struct {
		dst string
		gw  string
	}

	type nodeRoute struct {
		node   string
		routes []route
	}

	type testCase struct {
		path  string
		nodes []nodeRoute
	}

	tcs := []testCase{{
		path: "./test/data/simple-routes.b64",
		nodes: []nodeRoute{{
			node: "a0",
			routes: []route{
				{dst: "10.0.1.0/24", gw: "10.0.0.1"},
				{dst: "10.0.2.0/24", gw: "10.0.0.1"},
				{dst: "10.1.0.0/24", gw: "10.0.0.1"},
				{dst: "10.1.1.0/24", gw: "10.0.0.1"},
				{dst: "10.1.2.0/24", gw: "10.0.0.1"},
				{dst: "10.2.0.0/24", gw: "10.0.0.1"},
				{dst: "1.0.0.0/24", gw: "10.0.0.1"},
				{dst: "2.0.0.0/24", gw: "10.0.0.1"},
			},
		}, {
			node: "r1",
			routes: []route{
				{dst: "10.0.0.0/24", gw: "2.0.0.1"},
				{dst: "10.0.1.0/24", gw: "2.0.0.1"},
				{dst: "10.0.2.0/24", gw: "2.0.0.1"},
				{dst: "10.2.0.0/24", gw: "1.0.0.1"},
			},
		}},
	}, {
		path: "./test/data/multipath.b64",
		nodes: []nodeRoute{{
			node: "a",
			routes: []route{
				// a should have a route to every other non-nbr subnet
				{dst: "10.0.2.0/24", gw: "10.0.1.2"},
				{dst: "10.0.10.0/24", gw: "10.0.1.2"},
				{dst: "10.0.11.0/24", gw: "10.0.1.2"},
				{dst: "10.0.20.0/24", gw: "10.0.1.2"},
				{dst: "10.0.21.0/24", gw: "10.0.1.2"},
			},
		}, {
			node: "d",
			routes: []route{
				{dst: "10.0.1.0/24", gw: "10.0.2.1"},
				{dst: "10.0.10.0/24", gw: "10.0.2.1"},
				{dst: "10.0.11.0/24", gw: "10.0.2.1"},
				{dst: "10.0.20.0/24", gw: "10.0.2.1"},
				{dst: "10.0.21.0/24", gw: "10.0.2.1"},
			},
		}, {
			node: "gw1",
			routes: []route{
				{dst: "10.0.1.0/24", gw: "10.0.10.2"},
				{dst: "10.0.2.0/24", gw: "10.0.20.2"},
				{dst: "10.0.11.0/24", gw: "10.0.10.2"},
				{dst: "10.0.21.0/24", gw: "10.0.20.2"},
			},
		}},
	}, {
		path: "./test/data/simple-routes-layer2.b64",
		nodes: []nodeRoute{{
			node: "r1",
			routes: []route{
				{dst: "10.0.0.0/24", gw: "2.0.0.1"},
				{dst: "10.0.1.0/24", gw: "2.0.0.1"},
				{dst: "10.0.2.0/24", gw: "2.0.0.1"},
				{dst: "10.2.0.0/24", gw: "1.0.0.1"},
			},
		}, {
			node: "b2",
			routes: []route{
				{dst: "10.0.0.0/24", gw: "10.1.2.1"},
				{dst: "10.0.1.0/24", gw: "10.1.2.1"},
				{dst: "10.0.2.0/24", gw: "10.1.2.1"},
				{dst: "10.1.0.0/24", gw: "10.1.2.1"},
				{dst: "10.1.1.0/24", gw: "10.1.2.1"},
				{dst: "10.2.0.0/24", gw: "10.1.2.1"},
				{dst: "1.0.0.0/24", gw: "10.1.2.1"},
				{dst: "2.0.0.0/24", gw: "10.1.2.1"},
			},
		}},
	},
	}

	// Do the tests. Create the xir, run the reticulator and check expected results.

	for _, tCase := range tcs {

		log.Infof("Running Test Case: %s", tCase.path)

		net := readRoutingXirT(tCase.path, t)

		err := DoBuildStaticRoutes(net)
		if err != nil {
			t.Fatal(err)
		}

		for _, tn := range tCase.nodes {

			xirRoutes := net.GetNode(tn.node).Conf.Routes

			for _, tr := range tn.routes { // make sure all expected routes are there
				found := false
				for _, xr := range xirRoutes {
					if tr.gw == xr.Gw && tr.dst == xr.Dst {
						found = true
						break
					}
					t.Logf("found %s -> %s ~ %s in %s/%s routes",
						tn.node, xr.Dst, xr.Gw, tCase.path, tn.node)
				}
				if !found {
					t.Fatalf("In file %s for node %s expected route not found: %+v\nRoutes Found: %v",
						tCase.path, tn, tr, xirRoutes)
				}
			}
		}
	}
}

func TestStaticRoutingWithLayer2Nodes(t *testing.T) {

	net := readRoutingXirT("./test/data/simple-routes-layer2.b64", t)

	err := DoBuildStaticRoutes(net)
	if err != nil {
		t.Fatal(err)
	}

	// just confirm that a node that should have routes does and that the layer 2 node does not.

	routes := net.GetNode("b2").Conf.Routes
	if routes == nil {
		t.Fatalf("expected routes on node b2, found none")
	}

	if len(routes) != 8 {
		t.Fatalf("unexpected number of routes on b2: %d", len(routes))
	}

	n := net.GetNode("l2")

	if n.Conf != nil && n.Conf.Routes != nil && len(n.Conf.Routes) > 0 {
		t.Fatalf("found routes on a layer 2 node")
	}
}

func testStaticRoutingLinksSocketsIdentical(t *testing.T) {
	file := "./test/data/exp-gen64.b64"

	net1 := readRoutingXirT(file, t)
	err := doBuildStaticRoutesSockets(net1, runtime.NumCPU())
	if err != nil {
		t.Fatal(err)
	}

	log.Infof("sockets done")

	net2 := readRoutingXirT(file, t)
	err = doBuildStaticRoutesLinks(net2, runtime.NumCPU())
	if err != nil {
		t.Fatal(err)
	}

	log.Infof("links done")

	for _, node1 := range net1.Nodes {
		id := node1.Id

		node2 := net2.GetNode(id)

		routes1 := node1.GetConf().GetRoutes()
		routes2 := node2.GetConf().GetRoutes()

		if len(routes1) != len(routes2) {
			t.Errorf("number of routes not identical: %s: %d != %d", id, len(routes1), len(routes2))
		}

		// dest -> route
		rd := make(map[string]*xir.RouteConfig)
		for _, r1 := range routes1 {
			rd[r1.Dst] = r1
		}

		for _, r2 := range routes2 {
			r1 := rd[r2.Dst]
			if r1 == nil {
				t.Errorf("route in r2 not in r1: %s -> %s",
					id, r2.Dst)
			}

			if r1.Src != r2.Src {
				t.Errorf("route's src between r1 and r2 differ: %s -> %s: %s != %s",
					id, r2.Dst, r1.Src, r2.Src)
			}

			if r1.Gw != r2.Gw {
				t.Errorf("route's gw between r1 and r2 differ: %s -> %s: %s != %s",
					id, r2.Dst, r1.Gw, r2.Gw)
			}
		}
	}
}

func testStaticRoutingLinksMSTIdentical(t *testing.T) {
	file := "./test/data/exp-gen64.b64"

	net1 := readRoutingXirT(file, t)
	err := doBuildStaticRoutesLinks(net1, runtime.NumCPU())
	if err != nil {
		t.Fatal(err)
	}

	log.Infof("links done")

	net2 := readRoutingXirT(file, t)
	err = doBuildStaticRoutesMST(net2, runtime.NumCPU())
	if err != nil {
		t.Fatal(err)
	}

	log.Infof("MST done")

	for _, node1 := range net1.Nodes {
		id := node1.Id

		node2 := net2.GetNode(id)

		routes1 := node1.GetConf().GetRoutes()
		routes2 := node2.GetConf().GetRoutes()

		if len(routes1) != len(routes2) {
			t.Errorf("number of routes not identical: %s: %d != %d", id, len(routes1), len(routes2))
		}

		// dest -> route
		rd := make(map[string]*xir.RouteConfig)
		for _, r1 := range routes1 {
			rd[r1.Dst] = r1
		}

		for _, r2 := range routes2 {
			r1 := rd[r2.Dst]
			if r1 == nil {
				t.Errorf("route in r2 not in r1: %s -> %s",
					id, r2.Dst)
			}

			if r1.Src != r2.Src {
				t.Errorf("route's src between r1 and r2 differ: %s -> %s: %s != %s",
					id, r2.Dst, r1.Src, r2.Src)
			}

			if r1.Gw != r2.Gw {
				t.Errorf("route's gw between r1 and r2 differ: %s -> %s: %s != %s",
					id, r2.Dst, r1.Gw, r2.Gw)
			}
		}
	}
}

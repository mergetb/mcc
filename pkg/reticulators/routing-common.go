package reticulators

import (
	"fmt"
	"net/netip"
	"os"
	"runtime"
	"strconv"
	"strings"

	"github.com/puzpuzpuz/xsync/v3"
	log "github.com/sirupsen/logrus"

	xir "gitlab.com/mergetb/xir/v0.3/go"
)

// Sample xir ip/addr on node.
// ---------------------------
// nodes {
//   id: "s"
//   sockets {
//     addrs: "10.0.4.1/24"
//     endpoint {
//       element: "s.0~a0.2"
//     }
//   }
//   sockets {
//     index: 1
//     addrs: "10.0.5.1/24"
//     endpoint {
//       element: "s.1~b0.1"
//     }
//   }
// }

type netNode struct {
	Node *xir.Node
	IP   netip.Addr
	Net  netip.Prefix
	SIP  string
	SNet string
}

func socket2IPNetFirst(s *xir.Socket, m *xsync.MapOf[string, netNode]) (netNode, error) {
	if len(s.Addrs) == 0 {
		return netNode{}, fmt.Errorf("empty IP address list")
	}

	addr := s.Addrs[0]

	//m = nil

	if m == nil {
		prefix, err := netip.ParsePrefix(addr)
		if err != nil {
			return netNode{}, err
		}

		ip := prefix.Addr()
		net := prefix.Masked()

		return netNode{
			IP:   ip,
			Net:  net,
			SIP:  ip.String(),
			SNet: net.String(),
		}, nil
	}

	var err error

	node, _ := m.LoadOrCompute(
		addr,
		func() netNode {
			prefix, err2 := netip.ParsePrefix(addr)
			err = err2

			if err2 != nil {
				return netNode{}
			}

			ip := prefix.Addr()
			net := prefix.Masked()

			return netNode{
				IP:   ip,
				Net:  net,
				SIP:  ip.String(),
				SNet: net.String(),
			}
		},
	)

	if err != nil {
		return netNode{}, err
	}

	return node, nil

}

func (nn netNode) String() string {
	return fmt.Sprintf("{Node: %s, IP: %s, Net: %s}", nn.Node.GetId(), nn.IP, nn.Net)
}

type route struct {
	Src netNode
	Gw  netNode
	Dst netNode
}

func (r route) String() string {
	return fmt.Sprintf(
		"%6s -> %s@%s ~ %6s (%s -> %s ~ %s)",
		r.Src.Node.GetId(), r.Dst.Node.GetId(), r.Dst.IP, r.Gw.Node.GetId(),
		r.Src.IP, r.Dst.Net, r.Gw.IP,
	)
}

func DoBuildStaticRoutes(net *xir.Network) error {
	workers := 1
	workersEnv := os.Getenv("WORKERS")

	if workersEnv == "" {
		log.Debugf("using default workers of 1")
	} else {
		workersTemp, err := strconv.Atoi(workersEnv)
		if err != nil {
			return fmt.Errorf(
				"could not parse workers environment variable: \"%s\": %+v",
				workersEnv,
				err)
		}
		if err == nil {
			if workersTemp <= 0 {
				workers = runtime.NumCPU()
				log.Debugf("workers is <= 0, using NUM_CPU instead: %d", workers)
			} else {
				workers = workersTemp
				log.Debugf("workers: %d", workers)
			}

		}
	}

	method := os.Getenv("METHOD")

	switch strings.ToLower(method) {
	case "links":
		return doBuildStaticRoutesLinks(net, workers)
	case "sockets":
		return doBuildStaticRoutesSockets(net, workers)
	case "mst":
		return doBuildStaticRoutesMST(net, workers)
	}

	return doBuildStaticRoutesLinks(net, workers)

}

func nodesShareSubnet(l *netNode, r *netNode) (bool, error) {

	return l.Net.Overlaps(r.Net), nil
}

# pylint: skip-file
from mergexp import *

net = Network('multipath', routing==static)

a = net.node('a')
b = net.node('b')
c = net.node('c')
d = net.node('d')
gw1 = net.node('gw1')
gw2 = net.node('gw2')

net.connect([a, b])
a.sockets[0].addrs = ip4('10.0.1.1/24')
b.sockets[0].addrs = ip4('10.0.1.2/24')

net.connect([c, d])
c.sockets[0].addrs = ip4('10.0.2.1/24')
d.sockets[0].addrs = ip4('10.0.2.2/24')

net.connect([b, gw1])
b.sockets[1].addrs = ip4('10.0.10.2/24')
gw1.sockets[0].addrs = ip4('10.0.10.1/24')

net.connect([b, gw2])
b.sockets[2].addrs = ip4('10.0.11.2/24')
gw2.sockets[0].addrs = ip4('10.0.11.1/24')

net.connect([c, gw1])
c.sockets[1].addrs = ip4('10.0.20.2/24')
gw1.sockets[1].addrs = ip4('10.0.20.1/24')

net.connect([c, gw2])
c.sockets[2].addrs = ip4('10.0.21.2/24')
gw2.sockets[1].addrs = ip4('10.0.21.1/24')

experiment(net)

from mergexp import *

width = 47
lans = 47
nodes = 47

net = Network("large-topo", routing == static, addressing == ipv4)
hub = net.node("hub")

for w in range(width):

    root = net.node(f'hub-{w}')
    net.connect([root, hub])

    for l in range(lans):
        ns = [net.node(f'node{w}-{l}-{n}') for n in range(nodes)]
        net.connect([root] + ns)

experiment(net)


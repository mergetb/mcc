from mergexp import *

# start by creating a network object
net = Network('simple-routes', routing==static)

# it's just python, write your own node constructors
def node(name, group):
    dev = net.node(name)
    # dev.props['group'] = group
    return dev


# node definitions ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
alpha = [node(name, 1) for name in ['a0', 'a1', 'a2']]
bravo = [node(name, 2) for name in ['b0', 'b1', 'b2']]
charlie = [node(name, 3) for name in ['c0', 'c1', 'c2', 'cgw']]
routers = [node(name, 4) for name in ['r0', 'r1']]

l2 = node('l2', 5)

# connectivity ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# connect nodes
for i, a in enumerate(alpha):
    lnk = net.connect([a, routers[0]])
    # link[a].socket.addrs = ip4('10.0.0.1/24')
    lnk[a].socket.addrs = ip4('10.0.%d.10/24' % i)
    lnk[routers[0]].socket.addrs = ip4('10.0.%d.1/24' % i)

for i, b in enumerate(bravo):
    lnk = net.connect([b, routers[1]])
    lnk[b].socket.addrs = ip4('10.1.%d.10/24' % i)
    lnk[routers[1]].socket.addrs = ip4('10.1.%d.1/24' % i)

# connect nodes in 'LAN'
lnk = net.connect(charlie)

# gateway is special
# charlie[3].props['color'] = '#f00'
dmz = net.connect([charlie[3], routers[1]])

# charlie network
for i, x in enumerate(charlie, 2):
    x.sockets[0].addrs = ip4('10.2.0.%d/24' % i)

# routers - put a layer2 node between the two routers.
wanlink1 = net.connect([routers[0], l2], layer==2)
wanlink2 = net.connect([routers[1], l2], layer==2)
wanlink1[routers[0]].socket.addrs = ip4('2.0.0.1/24')
wanlink2[routers[1]].socket.addrs = ip4('2.0.0.2/24')

# dmz
dmz[charlie[3]].socket.addrs = ip4('1.0.0.1/24')
dmz[routers[1]].socket.addrs = ip4('1.0.0.2/24')

# package up to send to merge
experiment(net)

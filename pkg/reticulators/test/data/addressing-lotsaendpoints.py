import mergexp as mx

net = mx.Network('addrtest', mx.addressing == mx.ipv4)

a = [net.node(f'a{i}') for i in range(260)]
net.connect(a)

mx.experiment(net)

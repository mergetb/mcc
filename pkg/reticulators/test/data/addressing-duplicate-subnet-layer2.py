import mergexp as mx

net = mx.Network('addrtest', mx.addressing == mx.ipv4)

# Set one addr to 10.0.1.47 in lan A
a = [net.node('a%d'%i) for i in range(3)]
a_net = net.connect(a)
a_net[a[0]].socket.addrs = mx.ip4('10.0.1.47/24')

# Set one addr to 10.0.1.47 in lan B
b = [net.node('b%d'%i) for i in range(3)]
b_net = net.connect(b)
b_net[b[0]].socket.addrs = mx.ip4('10.0.1.48/24')

# single node to connect the lans together
c = net.node('c')
ca_net = net.connect([a[0], c], mx.layer==2)
cb_net = net.connect([b[0], c], mx.layer==2)

mx.experiment(net)

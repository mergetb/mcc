from mergexp import *

net = Network('addrtest', addressing==ipv4)

a = [net.node('a%d'%i) for i in range(3)]
net.connect([a[0], a[1]])
net.connect([a[0], a[2]])

b = [net.node('b%d'%i) for i in range(3)]
net_b = net.connect(b)

s = net.node('s')

s_a = net.connect([s, a[0]])
s_b = net.connect([s, b[0]])

experiment(net)

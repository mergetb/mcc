import mergexp as mx

net = mx.Network('addrtest', mx.addressing == mx.ipv4)

# make a big circle topology.
nodes = []
for i in range(260):

    nodes.append(net.node(f'a{i}'))
    if i == 0:
        continue

    net.connect([nodes[i-1], nodes[i]])

net.connect([nodes[len(nodes)-1], nodes[0]])

mx.experiment(net)

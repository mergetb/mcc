from mergexp import *

# start by creating a network object
net = Network('simple-routes', routing==static, addressing==ipv4)

# it's just python, write your own node constructors
def node(name, group):
    dev = net.node(name)
    # dev.props['group'] = group
    return dev


# node definitions ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
alpha = [node(name, 1) for name in ['a0', 'a1', 'a2', 'a3', 'a4']]
bravo = [node(name, 2) for name in ['b0', 'b1', 'b2', 'b3', 'b4']]
charlie = [node(name, 3) for name in ['c0', 'c1', 'c2', 'c3', 'c4']]

# connectivity ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
net.connect(alpha[:2])
net.connect(bravo[:2])
net.connect(charlie[:2])

net.connect(alpha[1:])
net.connect(bravo[1:])
net.connect(charlie[1:])

net.connect([alpha[0], bravo[0], charlie[0]])


# package up to send to merge
experiment(net)

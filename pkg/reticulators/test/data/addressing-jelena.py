#
# A specific model that gave the addressing reticultor trouble.
# This should be a valid model for auto-addressing.
#
from mergexp import *
# create a topology named 'Pingexp'
net = Network('Pingexp', addressing==ipv4)
nodes = []
nodes = nodes + [net.node(name, image=='Ubuntu-DEW') for name in [ 'client-1', 'client-2']]
nodes = nodes + [net.node(name, image=='Ubuntu-EDU') for name in [ 'server']]
lan0 = net.connect([nodes[2],nodes[0],nodes[1]], capacity==mbps(100), latency==ms(0))

lan0[nodes[1]].socket.addrs = ip4('1.1.1.3/24')
lan0[nodes[0]].socket.addrs = ip4('1.1.1.2/24')
experiment(net)

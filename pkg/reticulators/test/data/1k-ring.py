from mergexp import *

net = Network('circle', routing == static, addressing == ipv4)

nodes = 1000

prev = None
first = None

for i in range(nodes):
    n = net.node("n%04d" % i)
    if prev:
        net.connect([prev, n])
    else:
        first = n
    prev = n

net.connect([n, first])

experiment(net)

from mergexp import *

net = Network('addrtest', addressing==ipv4)

# The a net is not multilink.
a = [net.node('a%d'%i) for i in range(3)]
net.connect([a[0], a[1]])
a_net = net.connect([a[0], a[2]])
a_net[a[2]].socket.addrs = ip4('10.0.1.47/24')

# the b net is multilink
b = [net.node('b%d'%i) for i in range(3)]
net_b = net.connect(b)
net_b[b[0]].socket.addrs = ip4('172.0.47.1/24')

# the c net is multilink w/no addresses
c = [net.node('c%d'%i) for i in range(3)]
net.connect(c)

s = net.node('s')
net.connect([s, a[0]], layer==2)
net.connect([s, b[0]], layer==2)
net.connect([s, c[0]], layer==2)

experiment(net)
